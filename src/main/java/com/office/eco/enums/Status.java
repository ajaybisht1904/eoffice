package com.office.eco.enums;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:26:40 PM
 *
 * office
 */
public enum Status {

	CLOSED, DRAFT, IN_PROGRESS
}
