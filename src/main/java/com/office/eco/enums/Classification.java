package com.office.eco.enums;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:26:35 PM
 *
 * office
 */
public enum Classification {

	CONFIDENTIAL, RESTRICTED, UNCLASSIFIED
}
