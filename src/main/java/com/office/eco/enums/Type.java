package com.office.eco.enums;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:26:46 PM
 *
 * office
 */
public enum Type {
	TYPE_1, TYPE_2, TYPE_3
}
