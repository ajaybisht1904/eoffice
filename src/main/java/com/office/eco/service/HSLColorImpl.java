package com.office.eco.service;

import java.awt.Color;

public interface HSLColorImpl {

	String HSLToHex(float h, float s, float l);

	String pad(String s);

	String toHex(Color color);

	float HueToRGB(float p, float q, float h);

	Color toRGB(float h, float s, float l, float alpha);

	Color toRGB(float h, float s, float l);

}
