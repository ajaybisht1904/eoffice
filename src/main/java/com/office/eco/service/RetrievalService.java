package com.office.eco.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.office.eco.dto.AnnexureDto;
import com.office.eco.dto.InboxDTO;
import com.office.eco.dto.OutboxDTO;
import com.office.eco.dto.SendFileDTO;
import com.office.eco.models.PersonalApplicationInventory;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:29:10 PM
 *
 * office
 */
public interface RetrievalService {

	public void checkDraftFile(String id);
	public void checkFile(String id);
	public HashMap<String, Object> createFile(com.office.eco.dto.PersonalFileDto newFile);
	public HashMap<String, Object> sendFiles(String id, SendFileDTO sendFile, String roleName, String token, boolean signed);
	public HashMap<String, Object> getInboxData(String roleName,String userName, int pageSize , int pageNumber);
	boolean updateFile(MultipartFile file, String token, String idPersonal);
	HashMap<String, Object> getpersonalInfo(String username);
	boolean uploadAnnexureFile(MultipartFile[] file, String token, String userName, String roleName,
			String personalfileId);
	List<AnnexureDto> getAnnexureFile(String fileId);
	boolean uploadAnnotationData(String data, String personalFileId);
	String getAnnotationData(String id);
	 HashMap<String, Object> getOutboxData(String roleName, String userName, int pageSize , int pageNumber);
	HashMap<String, Object> getHrmData(String personalId);
	HashMap<String, Object> deleteAnnexureFile(String fileId);
	void createPersonalApplication(PersonalApplicationInventory newFile, String token, String roleName, String grp);
	HashMap<String, Object> getPANoting(String id, String roleName, String grp, String token) throws IOException, Exception;
	HashMap<String, Object> getPANotingData(String id) throws Exception;
	HashMap<String, Object> getPAEnclosureData(String ids,String id, String token, String roleName, String grp);
	HashMap<String, Object> sendFilesSection(SendFileDTO sendFile, String inboxId, String roleName, String token,
			boolean signed);
	HashMap<String, Object> sendFilesServiceNumber(SendFileDTO sendFile, String inboxId, String roleName, String token,
			boolean signed);
	HashMap<String, Object> updatePersonalInfo(String token, String role, String username, String address1, String address2, String address3,
			String name, String email, String designation);
	void createPartCaseFile(String token, String grp, String fileName, String username, String roleName,
			String applicationId, String inboxId) throws IOException;
	HashMap<String, Object> getPartCaseData(String id);
	boolean saveDocument(MultipartFile file, String id, String token, String roleName, boolean isPartCase)
			throws IOException;

}
