package com.office.eco.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public interface UpdateNoting {

	public ByteArrayOutputStream signStamp(String tag, String pencilColorCode, String comments, InputStream in ,String color,String username) throws IOException;
}
