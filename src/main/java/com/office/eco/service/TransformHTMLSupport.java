package com.office.eco.service;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public interface TransformHTMLSupport {

	void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException;

	void endElement(String uri, String localName, String qName) throws SAXException;

	void characters(char[] ch, int start, int length) throws SAXException;

	/**
	 * Get the Transformed HTML that can be feed to the DOCX converter.
	 */
	String getXMLForDOCX();

}
