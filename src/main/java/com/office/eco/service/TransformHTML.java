package com.office.eco.service;

import java.io.ByteArrayOutputStream;

public interface TransformHTML {
	public ByteArrayOutputStream transformParsing(String comments) throws Exception;
}
