package com.office.eco.service;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

import com.office.eco.dto.OutputBean1;

public interface SignNotingService {

	OutputBean1 signNoting(String comments, String signTitle, InputStream file,  String tag,
			String pencilColorCode, String username, String dep_desc, String color) throws IOException;

}
