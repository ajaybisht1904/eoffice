/**
 * 
 */
package com.office.eco.service;

import com.amazonaws.services.s3.AmazonS3;

/**
 * @author Aman Gupta
 *
 * Date : Nov 6, 2020 Time : 3:37:35 PM
 *
 * office
 */
public interface AWSClientConfig {

	/**
	 * @param token
	 * @return
	 */
	public AmazonS3 awsClientConfiguration(String token);

}
