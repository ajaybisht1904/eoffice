/**
 * 
 */
package com.office.eco.service;

import com.amazonaws.auth.BasicSessionCredentials;

/**
 * @author Aman Gupta
 *
 * Date : Nov 6, 2020 Time : 2:42:59 PM
 *
 * office
 */
public interface AWSConfig {

	/**
	 * @param token
	 * @return
	 */
	public BasicSessionCredentials client(String token);

}
