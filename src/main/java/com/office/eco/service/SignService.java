package com.office.eco.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

public interface SignService {

	ByteArrayOutputStream signdocs(String comments,InputStream file,String tag,String pencilColorCode,String username,String dep_desc,String color);

	boolean saveDocument(MultipartFile file) throws IOException;
}
