package com.office.eco.dto;

import java.time.LocalDateTime;
import org.bson.types.ObjectId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AnnexureDto {

 private String id;
 private String fileId;
 private String pfileName;
 private String fileUrl;
 private LocalDateTime createdOn;
}