package com.office.eco.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:26:12 PM
 *
 * office
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InboxDTO {

	private String createdOn;

	private String fileName;
	
	private String annotationId;

	private String id;
	
	private String personalApplicationInventoryId;
	
	private String fileid;
	
	private String partCase;
	
	private String personalUr;
	
	private String notingUrl;

	private String subject;

	private String type;
}
