package com.office.eco.dto;

import java.util.Arrays;

import org.springframework.web.multipart.MultipartFile;

public class SignedBean1 {
	public String comments = null;
	public String signTitle = null;
	public MultipartFile file = null;
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	//public byte[] filebytearray=null;
	public String tag = null;
	public String pencilColorCode = null;
	public String username=null;
	public String dep_desc=null;
	public String color=null;
	
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
//	public byte[] getFilebytearray() {
//		return filebytearray;
//	}
//	public void setFilebytearray(byte[] filebytearray) {
//		this.filebytearray = filebytearray;
//	}
	public String getDep_desc() {
		return dep_desc;
	}
	public void setDep_desc(String dep_desc) {
		this.dep_desc = dep_desc;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getSignTitle() {
		return signTitle;
	}
	public void setSignTitle(String signTitle) {
		this.signTitle = signTitle;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getPencilColorCode() {
		return pencilColorCode;
	}
	public void setPencilColorCode(String pencilColorCode) {
		this.pencilColorCode = pencilColorCode;
	}
	@Override
	public String toString() {
		return "SignedBean1 [comments=" + comments + ", signTitle=" + signTitle + ", tag=" + tag + ", pencilColorCode=" + pencilColorCode
				+ ", username=" + username + ", dep_desc=" + dep_desc + ", color=" + color + "]";
	}
	

	

	

}
