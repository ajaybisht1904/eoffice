package com.office.eco.dto;

import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:26:25 PM
 *
 * office
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SendFileDTO {

	private String comment;

	private String fileId;

	private ArrayList<CAU_SAU_Mapping> groups;
	
	private ArrayList<String> groupName;
	
	private ArrayList<String> serviceNumber;

	private Boolean nonOfficeUser;

	private String priority;

	private ArrayList<String> roles;

	// Imp
	private String status;

//	Imp
	private String userName;
	
	private String roleName;
	
	private String partCaseFile;

}
