package com.office.eco.dto;

import java.time.LocalDateTime;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:26:19 PM
 *
 * office
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OutboxDTO {

	private String comments;

	private String fileName;

	private String id;

	private String sentDate;

	private Integer status;

	private String subject;

	private List<String> to;

	private String type;

}
