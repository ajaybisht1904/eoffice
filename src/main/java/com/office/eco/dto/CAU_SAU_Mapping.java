package com.office.eco.dto;

import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CAU_SAU_Mapping {
	private int id;
	private String sau;
	private String sauDisplayName;
	private String coorduser;
}
