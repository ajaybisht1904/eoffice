package com.office.eco.dto;

import java.time.LocalDateTime;



import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PersonalApplicationEnclosureDTO {

	private String id;

	private String subject;

	private String status;
	
	private LocalDateTime createdOn;
	
	private String fileName;
	
	private String fileURL;

	private String userName;

	private String roleName;

	private String deptName;

	private String personalApplicationId;

}