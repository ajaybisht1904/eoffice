package com.office.eco.dto;

import java.time.LocalDateTime;
import org.bson.types.ObjectId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PersonalFileDto {

	private ObjectId id;

	private String subject;

	private String status;

	private LocalDateTime createdOn;

	private String pFileName;
	
	private String userName;
	
	private String roleName;
	
	private String grpName;
}
