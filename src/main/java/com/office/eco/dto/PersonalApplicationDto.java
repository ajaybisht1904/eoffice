package com.office.eco.dto;

import java.time.LocalDateTime;
import org.bson.types.ObjectId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PersonalApplicationDto {

 private String id;

 private String pfileName;
 private String subject;
 private String fileURL;
 
 private String status;
 private String createdOn;
 private String sentDate;
}