/**
 * 
 */
package com.office.eco.s3Store;

import org.springframework.content.s3.config.EnableS3Stores;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

/**
 * @author Aman Gupta
 *
 * Date : Nov 6, 2020 Time : 3:18:09 PM
 *
 * office
 */
@Configuration
@EnableS3Stores
public class AWSS3ContentService {

	@Bean
	public AmazonS3 client() {	
		@SuppressWarnings("deprecation")
		AmazonS3Client amazonS3Client = new AmazonS3Client(new AnonymousAWSCredentials());
		return amazonS3Client;
	}
}
