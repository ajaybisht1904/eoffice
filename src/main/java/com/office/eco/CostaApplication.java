package com.office.eco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CostaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CostaApplication.class, args);
	}

}
