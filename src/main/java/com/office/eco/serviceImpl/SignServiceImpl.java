package com.office.eco.serviceImpl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.office.eco.service.SignService;
import com.office.eco.service.UpdateNoting;

@Service
public class SignServiceImpl implements SignService{

	@Autowired
	private UpdateNoting updateNoting;

	@Override
	public ByteArrayOutputStream signdocs(String comments, InputStream file,
			String tag, String pencilColorCode, String username, String dep_desc, String color) {
		try {
			//System.out.println(file.getOriginalFilename());		
			ByteArrayOutputStream newFile;

			newFile = updateNoting.signStamp(tag, pencilColorCode, comments,file, pencilColorCode,username);


			if (newFile != null) {
				return newFile;
			}
			else {
				System.out.println("Signed failed");
				return null;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	@Override
	public boolean saveDocument(MultipartFile file) throws IOException
	{
		
		System.out.println(file.getContentType());
		System.out.println(file.getName());
		System.out.println(file.getSize());
		File f1 = new File("C:\\Users\\Rajat PC\\Desktop\\test\\text.docx");
		file.transferTo(f1);
		return true;
	}

}
