package com.office.eco.serviceImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import org.apache.commons.io.FileUtils;
import org.bson.types.ObjectId;
import org.camunda.bpm.client.ExternalTaskClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.policy.Policy;
import com.amazonaws.auth.policy.Principal;
import com.amazonaws.auth.policy.Resource;
import com.amazonaws.auth.policy.Statement;
import com.amazonaws.auth.policy.actions.S3Actions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectId;
import com.mongodb.client.result.UpdateResult;
import com.office.eco.camunda.externalTask.CamundaUtils;
import com.office.eco.dto.AnnexureDto;
import com.office.eco.dto.CAU_SAU_Mapping;
import com.office.eco.dto.InboxDTO;
import com.office.eco.dto.OutboxDTO;
import com.office.eco.dto.PersonalApplicationEnclosureDTO;
import com.office.eco.dto.PersonalApplicationNotingDTO;
import com.office.eco.dto.PersonalFileDto;
import com.office.eco.dto.SendFileDTO;
import com.office.eco.keycloakModels.KeycloakGroupRoleMapping;
import com.office.eco.keycloakModels.KeycloakGroups;
import com.office.eco.keycloakModels.KeycloakRoles;
import com.office.eco.keycloakRepo.KeycloakGroupRepository;
import com.office.eco.keycloakRepo.KeycloakGroupRoleMappingRepository;
import com.office.eco.keycloakRepo.KeycloakRolsRepository;
import com.office.eco.keycloakRepo.KeycloakUserGroupMembershipRepository;
import com.office.eco.keycloakRepo.KeycloakUsersRepository;
import com.office.eco.models.AnnotationData;
import com.office.eco.models.FileAnnexureInventory;
import com.office.eco.models.FileTrackInventory;
import com.office.eco.models.InboxCount;
import com.office.eco.models.InboxInventory;
import com.office.eco.models.OutboxCount;
import com.office.eco.models.OutboxInventory;
import com.office.eco.models.PartCaseEnclosureFile;
import com.office.eco.models.PartCaseInventory;
import com.office.eco.models.PartCaseNotingFile;
import com.office.eco.models.PersonalApplicationInventory;
import com.office.eco.models.PersonalApplicationNotingInventory;
import com.office.eco.models.PersonalFileInventory;
import com.office.eco.models.PersonalIdentityInventory;
import com.office.eco.repository.AnnotationDataRepository;
import com.office.eco.repository.FileAnnexureInventoryRepository;
import com.office.eco.repository.InboxInventoryRepository;
import com.office.eco.repository.OutboxInventoryRepository;
import com.office.eco.repository.PartCaseEnclosuresInventoryRepository;
import com.office.eco.repository.PartCaseInventoryRepository;
import com.office.eco.repository.PartCaseNotingInventoryRepository;
import com.office.eco.repository.PersonalApplicationNotingRepository;
import com.office.eco.repository.PersonalApplicationRepository;
import com.office.eco.repository.PersonalFileRepository;
import com.office.eco.repository.PersonalIdentityInventoryRepository;
import com.office.eco.service.AWSClientConfig;
import com.office.eco.service.RetrievalService;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:29:22 PM
 *
 * office
 */
@Service
public class RetrievalServiceImpl implements RetrievalService {


	//	AWS Services
	@Autowired	
	private PersonalFileRepository personalFileRepository;
	@Autowired
	private PersonalApplicationRepository personalApplicationRepository;
	@Autowired
	private AnnotationDataRepository annotationDataRepository;

	@Autowired
	private AWSClientConfig awsClientConfig;

	// Camunda Service
	@Autowired
	private CamundaUtils camundaUtils;

	@Autowired
	private PartCaseNotingInventoryRepository partCaseNotingInventoryRepository;

	@Autowired
	private PartCaseEnclosuresInventoryRepository partCaseEnclosuresInventoryRepository;

	@Autowired
	private PersonalIdentityInventoryRepository personalidentityInventoryRepository;
	@Autowired
	private FileAnnexureInventoryRepository fileAnnexureInventoryRepository;
	@Autowired
	private InboxInventoryRepository inboxInventoryRepository;
	@Autowired
	private PersonalApplicationNotingRepository personalApplicationNotingRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	private KeycloakUserGroupMembershipRepository keycloakUserGroupMembershipRepository;

	@Autowired
	private KeycloakGroupRepository keycloakGroupRepository;

	@Autowired
	private KeycloakGroupRoleMappingRepository keycloakGroupRoleMappingRepository;

	@Autowired
	private KeycloakUsersRepository keycloakUsersRepository;

	@Autowired
	private PartCaseInventoryRepository partCaseInventoryRepository;
	// Repository
	@Autowired
	private OutboxInventoryRepository outboxInventoryRepository;
	@Autowired
	private com.office.eco.repository.FileTrackInventoryRepository fileTrackInventoryRepository;

	@Value("${minio.rest-url}")
	private String baseUrl;

	@Value("${minio.rest-port}")
	private String port;

	HashMap<String, Object> responseObj = new HashMap<String, Object>(2);

	@Override
	public HashMap<String, Object> createFile(PersonalFileDto newFile) {
		responseObj.clear();
		List<PersonalFileInventory> countData = personalFileRepository.findByroleName(newFile.getRoleName());
		Optional<PersonalFileInventory> chkData = personalFileRepository.findByroleNameAndSubject(newFile.getRoleName(),newFile.getSubject());
		if(!chkData.isPresent())
		{
			ExternalTaskClient clientCamunda = camundaUtils.createExternalTask();
			clientCamunda.subscribe("pushPersonalData").handler((externalTask, externalTaskService) -> {
				String fileName = "IAF/"+ newFile.getUserName()+"/";
				Map<String, Object> variables = new HashMap<String, Object>(2);
				ObjectId id = new ObjectId();
				PersonalFileInventory newFileInventory = PersonalFileInventory.builder().id(id).subject(newFile.getSubject())
						.createdOn(LocalDateTime.now()).status("Initiate")
						.roleName(newFile.getRoleName()).build();
				if(!countData.isEmpty())
				{
					fileName = fileName.concat("1");		
					newFileInventory.setFileName(fileName);
					newFileInventory.setFileCount(1);
					personalFileRepository.save(newFileInventory);
					variables.put("status", "In Progress");
				}
				if(countData.isEmpty())
				{
					Long newCount = (long) (countData.size()+1);
					fileName = fileName.concat(newCount.toString());
					newFileInventory.setFileName(fileName);
					newFileInventory.setFileCount(newCount);
					personalFileRepository.save(newFileInventory);
					variables.put("status", "In Progress");
				}
				externalTaskService.complete(externalTask, variables);
				responseObj.put("message","Created Success !");
				responseObj.put("status",HttpStatus.OK);
			}).open();
		}
		responseObj.put("message","Already Exist !");
		responseObj.put("status",HttpStatus.NOT_FOUND);
		return responseObj;
	}

	@Override
	public HashMap<String, Object> getpersonalInfo(String username)
	{
		responseObj.clear();
		Optional<PersonalIdentityInventory> personalData = personalidentityInventoryRepository.findByUsername(username);
		if(personalData.isPresent())
		{
			responseObj.put("name",personalData.get().getName());
			responseObj.put("designation",personalData.get().getDesignation());
			responseObj.put("email",personalData.get().getEmail());
			responseObj.put("address1",personalData.get().getAddress1());
			responseObj.put("address2",personalData.get().getAddress2());
			responseObj.put("address3",personalData.get().getAddress3());
			responseObj.put("status","OK");
			return responseObj;
		}
		responseObj.put("status",HttpStatus.NOT_FOUND);
		return responseObj;
	}


	@Override
	public HashMap<String, Object> updatePersonalInfo(String token,String role,String username, String address1,String address2,String address3,String name,String email,String designation)
	{
		responseObj.clear();
		Optional<PersonalIdentityInventory> personalData = personalidentityInventoryRepository.findByUsername(username);
		if (personalData.isPresent())
		{

			Query query = new Query();
			query.addCriteria(Criteria.where("username").is(username));
			Update update = new Update();
			update.set("name", name);
			update.set("email", email);
			update.set("address1", address1);
			update.set("address2", address2);
			update.set("address3", address3);
			update.set("designation", designation);
			UpdateResult result = mongoTemplate.updateMulti(query, update, PersonalIdentityInventory.class);
			AmazonS3Client awsClient = (AmazonS3Client) awsClientConfig.awsClientConfiguration(token);
			if (!awsClient.doesBucketExist(role))
			{
				awsClient.createBucket(role);
				String policy = bucketPolicy(role);
				awsClient.setBucketPolicy(role,policy);
			}
			responseObj.put("status",HttpStatus.OK);
			return responseObj;

		}
		if (!personalData.isPresent())
		{

			PersonalIdentityInventory newInfo = PersonalIdentityInventory.builder().name(name).designation(designation)
					.address1(address1).address2(address2).address3(address3).email(email).username(username).build();
			personalidentityInventoryRepository.save(newInfo);
			responseObj.put("status",HttpStatus.OK);
			return responseObj;
		}
		responseObj.put("status",HttpStatus.NOT_FOUND);
		return responseObj;
	}

	@Autowired
	private KeycloakRolsRepository keycloakRolsRepository;

	@Override
	public HashMap<String, Object> sendFiles(String id, SendFileDTO sendFile,String roleName,String token,boolean signed) {
		responseObj.clear();
		ExternalTaskClient clientCamunda = camundaUtils.createExternalTask();
		ObjectId idGet = new ObjectId(id);
		Optional<PersonalApplicationInventory> initiateData = personalApplicationRepository.findById(idGet);
		ArrayList<CAU_SAU_Mapping> grpList;
		ArrayList<String> roleList;
		if (sendFile.getGroups() == null)
		{
			grpList = new ArrayList<CAU_SAU_Mapping>();
		}
		else
		{
			grpList = sendFile.getGroups();
		}
		if (sendFile.getRoles() == null)
		{
			roleList = new ArrayList<String>();
		}
		else
		{
			roleList = sendFile.getRoles();
		}
		clientCamunda.subscribe("sendFileMapping").handler((externalTask, externalTaskService) -> {

			if (grpList.isEmpty() == false) {
				for (CAU_SAU_Mapping grp : grpList) {
					List<String> userSentList = new ArrayList<String>();
					userSentList.add(sendFile.getUserName());
					ObjectId trackid = new ObjectId();
					Optional<KeycloakGroups> grpData = keycloakGroupRepository.findBygrpName(grp.getSau());
					List<KeycloakGroupRoleMapping> grpRoleData = keycloakGroupRoleMappingRepository.findByGroupId(grpData.get().getGrpId());
					List<String> rolesList = new ArrayList<String>();
					for (KeycloakGroupRoleMapping roleData:grpRoleData)
					{
						System.out.println(roleData.getRoleId());
						Optional<KeycloakRoles> rolesData = keycloakRolsRepository.findById(roleData.getRoleId());
						rolesList.add(rolesData.get().getRoleName());
					}
					List<String> roles = rolesList;
					Iterator itr = roles.iterator();
					while (itr.hasNext())
					{
						String x = (String) itr.next();
						if (x.equals(roleName))
							itr.remove();
					}
					FileTrackInventory fileTrack =  FileTrackInventory.builder().fileId(initiateData.get().getId().toString())
							.fileStatus("In-Progress").from(roleName).to(sendFile.getUserName()).trackId(trackid).build();
					fileTrackInventoryRepository.save(fileTrack);
					AmazonS3 awsClient = awsClientConfig.awsClientConfiguration(token);
					for(String rolee:roles)
					{
						String type = "";
						if(initiateData.get().isPartCase())
						{
							type = "File";
						}
						if(!initiateData.get().isPartCase())
						{
							type = "Personal Application";
						}
						InboxInventory inboxData = InboxInventory.builder()
								.personalApplicationInventoryId(initiateData.get().getId().toString()).trackId(trackid.toString())
								.roleName(rolee).type(type)
								.from(roleName).recieveDate(LocalDateTime.now()).isNewEntry(true)
								.status(3).build();

						OutboxInventory outboxData = OutboxInventory.builder().trackId(trackid.toString())
								.personalApplicationInventoryId(initiateData.get().getId().toString()).to(roles)
								.roleName(roleName).dateSent(LocalDateTime.now()).type(type)
								.isNewEntry(true).status(2).build();
						inboxInventoryRepository.save(inboxData);
						outboxInventoryRepository.save(outboxData);

						String url1 = initiateData.get().getFileURL();
						String keyUrl = url1.replaceAll(".*?\\/"+baseUrl+":"+port+"\\/.*?\\/","");
						if(keyUrl.contains("%20"))
						{
							keyUrl = keyUrl.replace("%20", " ");
						}
						System.out.println(awsClient.doesObjectExist(rolee, keyUrl));
						if(!awsClient.doesObjectExist(rolee, keyUrl))
						{
							awsClient.copyObject(roleName, keyUrl, rolee,keyUrl);
						}
					}
					Query query = new Query();
					query.addCriteria(Criteria.where("id").is(idGet));
					Update update = new Update();
					update.set("status", "Sent");
					UpdateResult result = mongoTemplate.updateMulti(query, update, PersonalApplicationInventory.class);
				}              

				responseObj.put("status", HttpStatus.OK);
			}
			Map<String, Object> variables = new HashMap<String, Object>();
			variables.put("status", "File Sent");
			externalTaskService.complete(externalTask, variables);

		}).open();
		return responseObj;


	}

	@Override
	public HashMap<String, Object> sendFilesSection(SendFileDTO sendFile, String inboxId,String roleName,String token,boolean signed) {
		responseObj.clear();
		//		ObjectId idGet = new ObjectId(inboxId); 
		//		ArrayList<String> grpList;
		//		ArrayList<String> roleList;
		//		Optional<InboxInventory> inboxData = inboxInventoryRepository.findById(idGet);
		//		ObjectId PAID = new ObjectId(inboxData.get().getPersonalApplicationInventoryId());
		//		Optional<PersonalApplicationInventory> initiateData = personalApplicationRepository.findById(PAID);
		//		if (sendFile.getGroups() == null)
		//		{
		//			//			grpList = new ArrayList<String>();
		//			grpList = sendFile.getGroupName();
		//			System.out.println(grpList);
		//		}
		//		else
		//		{
		//			//			grpList = sendFile.getGroupName();
		//			grpList = new ArrayList<String>();
		//		}
		//		if (sendFile.getRoles() == null)
		//		{
		//			roleList = new ArrayList<String>();
		//		}
		//		else
		//		{
		//			roleList = sendFile.getRoles();
		//		}
		//
		//
		//		if (grpList.isEmpty() == false) {
		//			for (String grp : grpList) {
		//				List<String> userSentList = new ArrayList<String>();
		//				userSentList.add(sendFile.getUserName());
		//				ObjectId trackid = new ObjectId();
		//				Optional<KeycloakGroups> grpData = keycloakGroupRepository.findBygrpName(grp);
		//
		//				List<KeycloakGroupRoleMapping> grpRoleData = keycloakGroupRoleMappingRepository.findByGroupId(grpData.get().getGrpId());
		//				List<String> rolesList = new ArrayList<String>();
		//				for (KeycloakGroupRoleMapping roleData:grpRoleData)
		//				{
		//					Optional<KeycloakRoles> rolesData = keycloakRolsRepository.findById(roleData.getRoleId());
		//					rolesList.add(rolesData.get().getRoleName());
		//				}
		//				List<String> roles = rolesList;
		//				Iterator itr = roles.iterator();
		//				while (itr.hasNext())
		//				{
		//					String x = (String) itr.next();
		//					if (x.equals(roleName))
		//						itr.remove();
		//				}
		//				FileTrackInventory fileTrack =  FileTrackInventory.builder().fileId(inboxData.get().getId().toString())
		//						.fileStatus("In-Progress").from(roleName).to(sendFile.getUserName()).trackId(trackid).build();
		//				fileTrackInventoryRepository.save(fileTrack);
		//				AmazonS3 awsClient = awsClientConfig.awsClientConfiguration(token);
		//				for(String rolee:roles)
		//				{
		//					String type = "";
		//					if(initiateData.get().isPartCase())
		//					{
		//						type = "File";
		//						Optional<PartCaseInventory> partFile = partCaseInventoryRepository.findById(sendFile.getPartCaseFile());
		//						if(partFile.isPresent())
		//						{
		//							HashMap<String, String> partNotingFile = partFile.get().getNotingFile();
		//
		//							for (HashMap.Entry<String,String> entry : partNotingFile.entrySet())
		//							{
		//								String url1 = entry.getValue();
		//								String keyUrl = url1.replaceAll(".*?\\/"+baseUrl+":"+port+"\\/.*?\\/","");
		//								if(keyUrl.contains("%20"))
		//								{
		//									keyUrl = keyUrl.replace("%20", " ");
		//								}
		//								if (!awsClient.doesBucketExist(rolee))
		//								{
		//
		//									awsClient.createBucket(rolee);
		//									String policy = bucketPolicy(rolee);
		//									awsClient.setBucketPolicy(rolee,policy);
		//
		//
		//								}
		//								if(!awsClient.doesObjectExist(rolee, keyUrl))
		//								{
		//									awsClient.copyObject(partFile.get().getRoleName(), keyUrl, rolee,keyUrl);
		//								}
		//								URL newUrls = awsClient.getUrl(rolee,keyUrl);
		//								System.out.println("Noting File");
		//								System.out.println("-->"+keyUrl);
		//								partNotingFile.replace(entry.getKey(), newUrls.toString());
		//
		//							}
		//							HashMap<String, String> partEnclosureFile = partFile.get().getEnclosureFile();
		//
		//							for (HashMap.Entry<String,String> entry1 : partEnclosureFile.entrySet())
		//							{
		//								String url1 = entry1.getValue();
		//								String keyUrl = url1.replaceAll(".*?\\/"+baseUrl+":"+port+"\\/.*?\\/","");
		//								if(keyUrl.contains("%20"))
		//								{
		//									keyUrl = keyUrl.replace("%20", " ");
		//								}
		//								if(!awsClient.doesObjectExist(rolee, keyUrl))
		//								{
		//									awsClient.copyObject(partFile.get().getRoleName(), keyUrl, rolee,keyUrl);
		//								}
		//								URL newUrls = awsClient.getUrl(rolee,keyUrl);
		//								System.out.println("Enclosure File");
		//								System.out.println("-->"+keyUrl);
		//								partEnclosureFile.replace(entry1.getKey(), newUrls.toString());
		//							}
		//							PartCaseInventory newPartCase = PartCaseInventory.builder().roleName(rolee).count(partFile.get().getCount()+1)
		//									.fileNumber(partFile.get().getFileNumber()).isNewEntry(true).personalApplicationId(partFile.get().getPersonalApplicationId())
		//									.enclosureFile(partEnclosureFile).notingFile(partNotingFile).notingFileId(partFile.get().getNotingFileId()).build();
		//							partCaseInventoryRepository.save(newPartCase);
		//							Optional<PartCaseInventory> newParts = partCaseInventoryRepository.findByFileNumberAndPersonalApplicationIdAndRoleName(partFile.get().getFileNumber(), partFile.get().getPersonalApplicationId(), rolee);
		//							InboxInventory inboxDatas = InboxInventory.builder()
		//									.personalApplicationInventoryId(inboxData.get().getPersonalApplicationInventoryId()).trackId(inboxData.get().getTrackId())
		//									.roleName(rolee).type(type).partCaseFile(newParts.get().getId())
		//									.from(roleName).recieveDate(LocalDateTime.now()).isNewEntry(true)
		//									.status(3).build();
		//							inboxInventoryRepository.save(inboxDatas);
		//						}
		//					}
		//					if(!initiateData.get().isPartCase())
		//					{
		//						type = "Personal Application";
		//						InboxInventory inboxDatas = InboxInventory.builder()
		//								.personalApplicationInventoryId(inboxData.get().getPersonalApplicationInventoryId()).trackId(inboxData.get().getTrackId())
		//								.roleName(rolee).type(type)
		//								.from(roleName).recieveDate(LocalDateTime.now()).isNewEntry(true)
		//								.status(3).build();
		//						inboxInventoryRepository.save(inboxDatas);
		//					}
		//
		//					OutboxInventory outboxData = OutboxInventory.builder()
		//							.personalApplicationInventoryId(inboxData.get().getPersonalApplicationInventoryId())
		//							.trackId(inboxData.get().getTrackId()).to(roles)
		//							.roleName(roleName).dateSent(LocalDateTime.now()).type(type)
		//							.isNewEntry(true).status(2).build();
		//
		//					outboxInventoryRepository.save(outboxData);
		//					inboxInventoryRepository.delete(inboxData.get());
		//					String PAFileURL = initiateData.get().getFileURL();
		//					//						String url1 = inboxData.get().getPersonalUrl();
		//					String url1 = PAFileURL;
		//
		//					String newUrl = url1.replaceAll(".*?\\/"+baseUrl+":"+port+"\\/.*?\\/","");
		//					if(newUrl.contains("%20"))
		//					{
		//						newUrl = newUrl.replace("%20", " ");
		//					}
		//					if(!awsClient.doesObjectExist(rolee, newUrl))
		//					{
		//						awsClient.copyObject(roleName, newUrl, rolee,newUrl);
		//					}
		//				}
		//				Query query = new Query();
		//				query.addCriteria(Criteria.where("id").is(idGet));
		//				Update update = new Update();
		//				update.set("status", "Sent");
		//				UpdateResult result = mongoTemplate.updateMulti(query, update, PersonalApplicationInventory.class);
		//			}              
		//
		//			responseObj.put("status", HttpStatus.OK);
		//		}

		return responseObj;


	}

	@Override
	public HashMap<String, Object> sendFilesServiceNumber(SendFileDTO sendFile, String inboxId,String roleName,String token,boolean signed) {
		responseObj.clear();
		//		ObjectId idGet = new ObjectId(inboxId); 
		//		ArrayList<String> serviceList;
		//		ArrayList<String> roleList;
		//		Optional<InboxInventory> inboxData = inboxInventoryRepository.findById(idGet);
		//		ObjectId PAID = new ObjectId(inboxData.get().getPersonalApplicationInventoryId());
		//		Optional<PersonalApplicationInventory> initiateData = personalApplicationRepository.findById(PAID);
		//		if (sendFile.getServiceNumber() != null)
		//		{
		//			serviceList = sendFile.getServiceNumber();
		//		}
		//		else
		//		{
		//			serviceList = new ArrayList<String>();
		//		}
		//		if (sendFile.getRoles() == null)
		//		{
		//			roleList = new ArrayList<String>();
		//		}
		//		else
		//		{
		//			roleList = sendFile.getRoles();
		//		}
		//
		//
		//		if (serviceList.isEmpty() == false) {
		//			for (String grp : serviceList) {
		//				List<String> userSentList = new ArrayList<String>();
		//				//					userSentList.add(grp);
		//				ObjectId trackid = new ObjectId();
		//				FileTrackInventory fileTrack =  FileTrackInventory.builder().fileId(inboxData.get().getId().toString())
		//						.fileStatus("In-Progress").from(roleName).to(sendFile.getUserName()).trackId(trackid).build();
		//				fileTrackInventoryRepository.save(fileTrack);
		//				AmazonS3 awsClient = awsClientConfig.awsClientConfiguration(token);
		//
		//				for(String rolee:serviceList)
		//				{
		//					Optional<KeycloakUsers> userRepo = keycloakUsersRepository.findByUserName(rolee);
		//					System.out.println("USER REPO : "+userRepo);
		//					String grpRepo= keycloakUserGroupMembershipRepository.findKeycloakUsersGroupByUserId(userRepo.get().getUserId());
		//					System.out.println("Group Repo : "+grpRepo);
		//					List<KeycloakGroupRoleMapping> roleRepo = keycloakGroupRoleMappingRepository.findByGroupId(grpRepo);
		//					System.out.println(roleRepo);
		//
		//					for(KeycloakGroupRoleMapping roleIds:roleRepo)
		//					{
		//						Optional<KeycloakRoles> rolesIds = keycloakRolsRepository.findByRoleId(roleIds.getRoleId());
		//						String type = "";
		//						if(initiateData.get().isPartCase())
		//						{
		//							type = "File";
		//							Optional<PartCaseInventory> partFile = partCaseInventoryRepository.findById(sendFile.getPartCaseFile());
		//							if(partFile.isPresent())
		//							{
		//								HashMap<String, String> partNotingFile = partFile.get().getNotingFile();
		//
		//								for (HashMap.Entry<String,String> entry : partNotingFile.entrySet())
		//								{
		//									String url1 = entry.getValue();
		//									String keyUrl = url1.replaceAll(".*?\\/"+baseUrl+":"+port+"\\/.*?\\/","");
		//									if(keyUrl.contains("%20"))
		//									{
		//										keyUrl = keyUrl.replace("%20", " ");
		//									}
		//									System.out.println("Bucket Name"+rolesIds.get().getRoleName());
		//
		//									if(!awsClient.doesObjectExist(rolesIds.get().getRoleName(), keyUrl))
		//									{
		//										awsClient.copyObject(partFile.get().getRoleName(), keyUrl, rolesIds.get().getRoleName(),keyUrl);
		//									}
		//									URL newUrls = awsClient.getUrl(rolesIds.get().getRoleName(),keyUrl);
		//									partNotingFile.replace(entry.getKey(), newUrls.toString());
		//								}
		//								HashMap<String, String> partEnclosureFile = partFile.get().getEnclosureFile();
		//
		//								for (HashMap.Entry<String,String> entry1 : partEnclosureFile.entrySet())
		//								{
		//									String url1 = entry1.getValue();
		//									String keyUrl = url1.replaceAll(".*?\\/"+baseUrl+":"+port+"\\/.*?\\/","");
		//									if(keyUrl.contains("%20"))
		//									{
		//										keyUrl = keyUrl.replace("%20", " ");
		//									}
		//									if (!awsClient.doesBucketExist(rolesIds.get().getRoleName()))
		//									{
		//
		//										awsClient.createBucket(rolesIds.get().getRoleName());
		//										String policy = bucketPolicy(rolesIds.get().getRoleName());
		//										awsClient.setBucketPolicy(rolesIds.get().getRoleName(),policy);
		//
		//
		//									}
		//									if(!awsClient.doesObjectExist(rolesIds.get().getRoleName(), keyUrl))
		//									{
		//										awsClient.copyObject(partFile.get().getRoleName(), keyUrl, rolesIds.get().getRoleName(),keyUrl);
		//									}
		//									URL newUrls = awsClient.getUrl(rolesIds.get().getRoleName(),keyUrl);
		//									partEnclosureFile.replace(entry1.getKey(), newUrls.toString());
		//								}
		//								PartCaseInventory newPartCase = PartCaseInventory.builder().roleName(rolesIds.get().getRoleName()).count(partFile.get().getCount()+1)
		//										.fileNumber(partFile.get().getFileNumber()).notingFileId(partFile.get().getNotingFileId()).isNewEntry(true).personalApplicationId(partFile.get().getPersonalApplicationId())
		//										.enclosureFile(partEnclosureFile).notingFile(partNotingFile).build();
		//								partCaseInventoryRepository.save(newPartCase);
		//								Optional<PartCaseInventory> newParts = partCaseInventoryRepository.findByFileNumberAndPersonalApplicationIdAndRoleName(partFile.get().getFileNumber(), partFile.get().getPersonalApplicationId(), rolesIds.get().getRoleName());
		//								InboxInventory inboxDatas = InboxInventory.builder()
		//										.personalApplicationInventoryId(inboxData.get().getPersonalApplicationInventoryId()).trackId(inboxData.get().getTrackId())
		//										.roleName(rolesIds.get().getRoleName()).type(type).partCaseFile(newParts.get().getId())
		//										.from(roleName).recieveDate(LocalDateTime.now()).isNewEntry(true)
		//										.status(3).build();
		//								inboxInventoryRepository.save(inboxDatas);
		//							}
		//						}
		//
		//						if(!initiateData.get().isPartCase())
		//						{
		//
		//							type = "Personal Application";
		//							InboxInventory inboxDatas = InboxInventory.builder()
		//									.personalApplicationInventoryId(inboxData.get().getPersonalApplicationInventoryId()).trackId(inboxData.get().getTrackId())
		//									.roleName(rolesIds.get().getRoleName()).type(type)
		//									.from(roleName).recieveDate(LocalDateTime.now()).isNewEntry(true)
		//									.status(3).build();
		//							inboxInventoryRepository.save(inboxDatas);
		//						}
		//
		//						userSentList.add(rolesIds.get().getRoleName());
		//						OutboxInventory outboxData = OutboxInventory.builder()
		//								.personalApplicationInventoryId(inboxData.get().getPersonalApplicationInventoryId())
		//								.trackId(inboxData.get().getTrackId()).to(userSentList)
		//								.roleName(roleName).dateSent(LocalDateTime.now()).type(type)
		//								.isNewEntry(true).status(2).build();
		//
		//						outboxInventoryRepository.save(outboxData);
		//						inboxInventoryRepository.delete(inboxData.get());
		//						String PAFileURL = initiateData.get().getFileURL();
		//						String url1 = PAFileURL;
		//
		//						String newUrl = url1.replaceAll(".*?\\/"+baseUrl+":"+port+"\\/.*?\\/","");
		//						if(newUrl.contains("%20"))
		//						{
		//							newUrl = newUrl.replace("%20", " ");
		//						}
		//						if(!awsClient.doesObjectExist(rolesIds.get().getRoleName(), newUrl))
		//						{
		//							awsClient.copyObject(roleName, newUrl, rolesIds.get().getRoleName(),newUrl);
		//						}
		//					}
		//					Query query = new Query();
		//					query.addCriteria(Criteria.where("id").is(idGet));
		//					Update update = new Update();
		//					update.set("status", "Sent");
		//					UpdateResult result = mongoTemplate.updateMulti(query, update, PersonalApplicationInventory.class);
		//				}
		//			}
		//
		//			responseObj.put("status", HttpStatus.OK);
		//		}

		return responseObj;


	}


	@Override
	public void createPersonalApplication(PersonalApplicationInventory newFile, String token, String roleName,String grp) {
		ExternalTaskClient clientCamunda = camundaUtils.createExternalTask();
		clientCamunda.subscribe("pushPersonalApplicationData").handler((externalTask, externalTaskService) -> {
			ObjectId id = new ObjectId();
			AmazonS3 awsClient = awsClientConfig.awsClientConfiguration(token);
			S3ObjectId objectId =  new S3ObjectId(roleName, id.toString());
			if (!awsClient.doesBucketExist(roleName))
			{

				awsClient.createBucket(roleName);
				String policy = bucketPolicy(roleName);
				awsClient.setBucketPolicy(roleName,policy);


			}
			URL url = null;
			awsClient.copyObject("template", "PersonalTemplate.docx", roleName, grp+"/"+newFile.getSubject()+"/"+newFile.getPfileName()+"/PersonalTemplate.docx");
			url = awsClient.getUrl(roleName,grp+"/"+newFile.getSubject()+"/"+newFile.getPfileName()+"/PersonalTemplate.docx");
			System.out.println(newFile);
			PersonalApplicationInventory newFileInventory = PersonalApplicationInventory.builder().id(id).subject(newFile.getSubject())
					.createdOn(LocalDateTime.now()).pfileName(newFile.getPfileName()).status("In Progress")
					.roleName(newFile.getRoleName()).fileURL(url.toString()).isPartCase(false).build();

			personalApplicationRepository.save(newFileInventory);
			Map<String, Object> variables = new HashMap<String, Object>();
			variables.put("grp_name", grp);
			variables.put("status", "In-Progress");
			variables.put("fileId", "In-Progress");
			externalTaskService.complete(externalTask, variables);
		}).open();

	}

	@Override
	public void checkDraftFile(String id) {

	}

	@Override
	public void checkFile(String id) {

	}

	//	@Override
	//	public void createFile(CreateFileDTO newFile) {
	//		List<InitiateInventory> newFileInitiate = new ArrayList<InitiateInventory>();
	//		ExternalTaskClient clientCamunda = camundaUtils.createExternalTask();
	//		int series = 000;
	//		clientCamunda.subscribe("metadata_mapping").handler((externalTask, externalTaskService) -> {
	//
	//			String fileName = newFile.getDepartment() + "/" + newFile.getSection() + "/"
	//					+ series + 1;
	//			ObjectId id = new ObjectId();
	//			FileInventory newFileInventory = FileInventory.builder().id(id).subject(newFile.getSubject())
	//					.classification(newFile.getClassification())
	//					.dept(newFile.getDepartment()).section(newFile.getSection())
	//					.type(newFile.getType()).fileName(fileName).build();
	//			fileInventoryRepository.save(newFileInventory);
	//			InitiateInventory newInventory = InitiateInventory.builder().createdDate(LocalDateTime.now())
	//					.fileInventoryId(id.toString()).roleName(newFile.getRoleName())
	//					.statusId(1).build();
	//			Map<String, Object> variables = new HashMap<String, Object>();
	//			variables.put("username", newFile.getUserName());
	//			variables.put("status", "In-Progress");
	//			variables.put("fileId", "In-Progress");
	//			externalTaskService.complete(externalTask, variables);
	//			newFileInitiate.add(newInventory);
	//			initiateInventoryRepository.save(newInventory);
	//		}).open();
	//
	//	}

	@Override
	public boolean saveDocument(MultipartFile file, String id,String token,String roleName,boolean isPartCase) throws IOException
	{
		if(!isPartCase)
		{
		ObjectId oldId = new ObjectId(id);
		Optional<PersonalApplicationInventory> checkFile = personalApplicationRepository.findById(oldId);
		if (checkFile.isPresent())
		{
			System.out.println("In Sign Service");

			AmazonS3 awsClient = awsClientConfig.awsClientConfiguration(token);
			URL url = null;
			String clipUrl  = checkFile.get().getFileURL();
			String newUrl = clipUrl.replaceAll(".*?\\/"+baseUrl+":"+port+"\\/.*?\\/","");
			if(newUrl.contains("%20"))
			{
				newUrl = newUrl.replace("%20", " ");
			}
			if (!awsClient.doesBucketExist(roleName))
			{
				awsClient.createBucket(roleName);
				String policy = bucketPolicy(roleName);
				awsClient.setBucketPolicy(roleName,policy);

			}

			System.out.println("Inside If Condition "+file);
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentType(file.getContentType());
			metadata.setContentLength(file.getSize());
			String newURL = newUrl.replace("%20", " ");


			DeleteObjectRequest requestDel = new DeleteObjectRequest(roleName, newUrl); 
			System.out.println("Logging");
			PutObjectRequest request = new PutObjectRequest(roleName, newUrl, file.getInputStream(),metadata);
			awsClient.putObject(request);
			return true;

		}
		//		ObjectId oldIds = new ObjectId(id);
		
		Optional<PersonalApplicationNotingInventory> notingFile = personalApplicationNotingRepository.findById(oldId);
		if(notingFile.isPresent())
		{ 
			System.out.println("In Sign Service 1"+oldId);

			AmazonS3 awsClient = awsClientConfig.awsClientConfiguration(token);
			URL url = null;
			String clipUrl  = notingFile.get().getFileURL();
			String newUrl = clipUrl.replaceAll(".*?\\/"+baseUrl+":"+port+"\\/.*?\\/","");
			if(newUrl.contains("%20"))
			{
				newUrl = newUrl.replace("%20", " ");
			}
			if (!awsClient.doesBucketExist(roleName))
			{
				awsClient.createBucket(roleName);
				String policy = bucketPolicy(roleName);
				awsClient.setBucketPolicy(roleName,policy);

			}

			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentType(file.getContentType());
			metadata.setContentLength(file.getSize());

			DeleteObjectRequest requestDel = new DeleteObjectRequest(roleName, newUrl); 
			PutObjectRequest request = new PutObjectRequest(roleName, newUrl, file.getInputStream(),metadata);
			awsClient.putObject(request);
			return true;
		}
		}
		if(isPartCase)
		{
		Optional<PartCaseNotingFile> partCaseNotingFile = partCaseNotingInventoryRepository.findByFileId(id);
		
		if(partCaseNotingFile.isPresent())
		{ 
			
			AmazonS3 awsClient = awsClientConfig.awsClientConfiguration(token);
			URL url = null;
			String clipUrl  = partCaseNotingFile.get().getFileUrl();
			String newUrl = clipUrl.replaceAll(".*?\\/"+baseUrl+":"+port+"\\/.*?\\/","");
			if(newUrl.contains("%20"))
			{
				newUrl = newUrl.replace("%20", " ");
			}
			if(newUrl.contains("%28"))
			{
				newUrl = newUrl.replace("%28", "(");
			}
			if(newUrl.contains("%29"))
			{
				newUrl = newUrl.replace("%29", ")");
			}
			System.out.println(newUrl);
			if (!awsClient.doesBucketExist(roleName))
			{

				awsClient.createBucket(roleName);
				String policy = bucketPolicy(roleName);
				awsClient.setBucketPolicy(roleName,policy);

			}

			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentType(file.getContentType());
			metadata.setContentLength(file.getSize());

			DeleteObjectRequest requestDel = new DeleteObjectRequest(roleName, newUrl); 
			PutObjectRequest request = new PutObjectRequest(roleName, newUrl, file.getInputStream(),metadata);
			System.out.println("Saved");
			awsClient.putObject(request);
			return true;
		}
		}
		return false;
	}


	@Override
	public List<AnnexureDto> getAnnexureFile(String fileId)
	{
		List<FileAnnexureInventory> fileData = fileAnnexureInventoryRepository.findByPersonalFileId(fileId);
		List<AnnexureDto> sortedList = new ArrayList<AnnexureDto>();
		List<AnnexureDto> annexureList = fileData.stream().map(data ->{
			AnnexureDto dto = AnnexureDto.builder().createdOn(data.getUploadingDate()).pfileName(data.getOriginalFileName())
					.fileUrl(data.getAnnexureFileURL()).id(data.getId().toString()).build();
			return dto;

		}).collect(Collectors.toList());
		sortedList = annexureList.stream()
				.sorted(Comparator.comparing(AnnexureDto :: getCreatedOn).reversed())
				.collect(Collectors.toList());

		return sortedList;
	}

	@Override
	public boolean uploadAnnexureFile(MultipartFile[] file, String token, String userName, String roleName,String personalfileId) {
		try {
			ObjectId oldId = new ObjectId(personalfileId);
			Optional<PersonalApplicationInventory> personalFileData = personalApplicationRepository.findById(oldId);

			if(personalFileData.isPresent())
			{
				ObjectId id = new ObjectId();
				for (MultipartFile files:file)
				{

					AmazonS3 awsClient = awsClientConfig.awsClientConfiguration(token);
					S3ObjectId objectId =  new S3ObjectId(roleName, id.toString());
					if (!awsClient.doesBucketExist(roleName))
					{
						awsClient.createBucket(roleName);
						String policy = bucketPolicy(roleName);
						awsClient.setBucketPolicy(roleName,policy);

					}
					ObjectMetadata metadata = new ObjectMetadata();
					metadata.setContentType(files.getContentType());
					metadata.setContentLength(files.getSize());
					PutObjectRequest request = new PutObjectRequest(roleName,  "CC/PersonalApplication/Annexure/"+personalFileData.get().getPfileName()+"/"+personalFileData.get().getSubject()+"/"+files.getOriginalFilename(), files.getInputStream(),metadata);
					awsClient.putObject(request);
					URL annexureFileUrl = awsClient.getUrl(roleName,  "CC/PersonalApplication/Annexure/"+personalFileData.get().getPfileName()+"/"+personalFileData.get().getSubject()+"/"+files.getOriginalFilename());

					FileAnnexureInventory newFile = FileAnnexureInventory.builder().by(userName).contentId(id.toString())
							.contentLength(files.getSize()).fileName(files.getName()).mimeType(files.getContentType())
							.originalFileName(files.getOriginalFilename()).personalFileId(personalfileId).uploadingDate(LocalDateTime.now())
							.annexureFileURL(annexureFileUrl.toString()).build();
					fileAnnexureInventoryRepository.save(newFile);

				}

			}
			return true;
		}
		catch (Exception e) {
			System.out.println("Error occurred: " + e);
			return false;
		}
	}


	@Override
	public HashMap<String, Object> getOutboxData(String roleName,String userName, int pageSize, int pageNumber)
	{
		responseObj.clear();
		Pageable paging = PageRequest.of(pageNumber, pageSize);
	      
		Page<OutboxInventory> outboxData = outboxInventoryRepository
				.findAllByRoleNameOrderByDateSent(roleName, paging);
		Aggregation agg = newAggregation(
				match(Criteria.where("roleName").is(roleName)),
				group("roleName").count().as("total")
					
			);
//		List<OutboxInventory> outboxData = outboxInventoryRepository
//				.findAllByRoleNameOrderByDateSent(roleName);
		AggregationResults<OutboxCount> groupResults 
		= mongoTemplate.aggregate(agg, OutboxInventory.class,OutboxCount.class);
	List<OutboxCount> result = groupResults.getMappedResults();

	for(OutboxCount res:result)
	{
		
		responseObj.put("length", res.getTotal());
	}
		List<OutboxDTO> sortedList = new ArrayList<OutboxDTO>();
		if (!outboxData.isEmpty())
		{
			List<OutboxDTO> outboxDataList = outboxData.stream().map(data ->
			{

				OutboxDTO newDraft = new OutboxDTO();
				ObjectId newId = new ObjectId(data.getPersonalApplicationInventoryId());
				Optional<PersonalApplicationInventory> fileNewData = personalApplicationRepository.findById(newId);
				LocalDateTime createdOn = data.getDateSent();
				DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");  
				String formatDateTime = createdOn.format(format); 
				newDraft.setId(data.getId().toString());
				newDraft.setTo(data.getTo());
				newDraft.setSentDate(formatDateTime);
				String type = "";
				if(fileNewData.get().isPartCase())
				{
					type = "File";
				}
				if(!fileNewData.get().isPartCase())
				{
					type = "Personal Application";
				}
				newDraft.setType(type);
				newDraft.setFileName(fileNewData.get().getPfileName());
				newDraft.setSubject(fileNewData.get().getSubject());
				newDraft.setStatus(2);
				return newDraft;
			}).collect(Collectors.toList());
			sortedList = outboxDataList.stream()
					.sorted(Comparator.comparing(OutboxDTO :: getSentDate).reversed())
					.collect(Collectors.toList());

			responseObj.put("Data", outboxDataList);
			return responseObj;
		}

		return responseObj;
	}

	@Override
	public HashMap<String, Object> getHrmData(String personalId)
	{
		responseObj.clear();
		ObjectId newId = new ObjectId(personalId);
		Optional<PersonalApplicationInventory> persoanlData = personalApplicationRepository.findById(newId);

		if(persoanlData.isPresent())
		{
			List<FileAnnexureInventory> annexureData = fileAnnexureInventoryRepository.findByPersonalFileId(personalId);
			responseObj.put("personalData", persoanlData.get());
			responseObj.put("annexureData", annexureData);

			return responseObj;

		}

		return new HashMap<String, Object>();
	}

	@Override
	public HashMap<String, Object> getInboxData(String roleName,String userName, int pageSize, int pageNumber)
	{
		responseObj.clear();
		Pageable paging = PageRequest.of(pageNumber, pageSize);
	      
		Page<InboxInventory> inboxData = inboxInventoryRepository
				.findAllByRoleNameOrderByRecieveDate(roleName, paging);
		Aggregation agg = newAggregation(
				match(Criteria.where("roleName").is(roleName)),
				group("roleName").count().as("total")
					
			);

			//Convert the aggregation result into a List
			AggregationResults<InboxCount> groupResults 
				= mongoTemplate.aggregate(agg, InboxInventory.class,InboxCount.class);
			List<InboxCount> result = groupResults.getMappedResults();
		
		for(InboxCount res:result)
		{
			
			responseObj.put("length", res.getTotal());
		}
		List<InboxDTO> sortedList = new ArrayList<InboxDTO>();
		if (!inboxData.isEmpty())
		{
			List<InboxDTO> inboxDataList = inboxData.stream().map(data -> 
			{
				InboxDTO newDraft = new InboxDTO();

				ObjectId newId = new ObjectId(data.getPersonalApplicationInventoryId());
				LocalDateTime createdOn = data.getRecieveDate();
				DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");  
				String formatDateTime = createdOn.format(format);   
				Optional<PersonalApplicationInventory> fileNewData = personalApplicationRepository.findById(newId);

				newDraft.setId(data.getId().toString());
				if(data.getPartCaseFile() != null)
				{
					newDraft.setPartCase(data.getPartCaseFile());
				}
				newDraft.setCreatedOn(formatDateTime);

				newDraft.setSubject(fileNewData.get().getSubject());
				newDraft.setAnnotationId(fileNewData.get().getAnnotationId());
				newDraft.setPersonalApplicationInventoryId(data.getPersonalApplicationInventoryId());
				String type = "";
				if(fileNewData.get().isPartCase() && data.getPartCaseFile() != null)
				{
					type = "File";
					Optional<PartCaseInventory> partCaseData = partCaseInventoryRepository.findById(data.getPartCaseFile());
					if(partCaseData.isPresent())
					{

						Optional<PartCaseInventory> partCaseData1 = partCaseInventoryRepository.findById(partCaseData.get().getId().toString());
						if(partCaseData1.isPresent())
						{
							ObjectId oldId = new ObjectId(partCaseData1.get().getNotingFileId());
						Optional<PartCaseNotingFile> notingFileD = partCaseNotingInventoryRepository.findById(oldId);
						if(notingFileD.isPresent())
						{
							System.out.println(notingFileD.get().getFileUrl());
							newDraft.setFileName(notingFileD.get().getFileUrl());
						}
						}
					}
				}
				if(!fileNewData.get().isPartCase())
				{
					type = "Personal Application";
					newDraft.setFileName(fileNewData.get().getFileURL());
				}
				newDraft.setType(type);
				return newDraft;
			}).collect(Collectors.toList());
			sortedList = inboxDataList.stream()
					.sorted(Comparator.comparing(InboxDTO :: getCreatedOn).reversed())
					.collect(Collectors.toList());
			
			responseObj.put("Data", inboxDataList);
			return responseObj;
		}

		return responseObj;
	}

	@Override
	public boolean uploadAnnotationData(String data,String personalFileId)
	{
		ObjectId newId1 = new ObjectId(personalFileId);
		Optional<PersonalApplicationInventory> fileData = personalApplicationRepository.findById(newId1);
		if (fileData.isPresent())
		{
			ObjectId newId =  new ObjectId();
			AnnotationData newData = AnnotationData.builder().id(newId).annotationData(data).build();
			PersonalApplicationInventory file = mongoTemplate.findOne(Query.query(Criteria.where("id").is(personalFileId)), PersonalApplicationInventory.class);
			file.setAnnotationId(newId.toString());
			mongoTemplate.save(file);
			annotationDataRepository.save(newData);
			return true;
		}
		return false;

	}

	@Override
	public String getAnnotationData(String id) {
		responseObj.clear();
		ObjectId annotId = new ObjectId(id);
		Optional<AnnotationData> annotData = annotationDataRepository.findById(annotId);
		String xsdfString = annotData.get().getAnnotationData();
		return xsdfString;
	}
	@Override
	public boolean updateFile(MultipartFile file,String token, String idPersonal) {
		responseObj.clear();
		try {
			ObjectId newId = new ObjectId(idPersonal); 
			Optional<PersonalApplicationInventory> fileData = personalApplicationRepository.findById(newId);

			AmazonS3 awsClient = awsClientConfig.awsClientConfiguration(token);
			String type = fileData.get().getType();

			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentType(file.getContentType());
			metadata.setContentLength(file.getSize());
			PutObjectRequest request = new PutObjectRequest("template",  "/PersonalTemplate.docx",file.getInputStream(),metadata);
			awsClient.putObject(request);
			return true;
		}
		catch (Exception e) {
			System.out.println("Error occurred: " + e);
			return false;
		}
	}

	@Override
	public HashMap<String, Object> deleteAnnexureFile(String fileId)
	{
		responseObj.clear();
		ObjectId id = new ObjectId(fileId);
		fileAnnexureInventoryRepository.deleteById(id);

		responseObj.put("status", HttpStatus.OK);
		return responseObj;
	}

	@Override
	public HashMap<String, Object> getPANotingData(String id) throws Exception
	{
		responseObj.clear();
		ObjectId ids = new ObjectId(id);
		Optional<PersonalApplicationInventory> appData = personalApplicationRepository.findById(ids);
		List<String> notingData = new ArrayList<String>();
		if(appData.isPresent())
		{
			Optional<PersonalApplicationNotingInventory> paNotingData = personalApplicationNotingRepository.findByPersonalApplicationIdAndFileName(id,appData.get().getPfileName());
			if(paNotingData.isPresent())
			{
				String notingUrl = paNotingData.get().getFileURL();
				notingUrl = notingUrl.replace("%28", "(");
				notingUrl = notingUrl.replace("%29", ")");
				PersonalApplicationNotingDTO paNotingDto = PersonalApplicationNotingDTO.builder().id(paNotingData.get().getId().toString())
						.deptName(paNotingData.get().getDeptName()).roleName(paNotingData.get().getRoleName())
						.fileName(paNotingData.get().getFileName()).fileURL(notingUrl)
						.subject(paNotingData.get().getSubject()).build();
				URL urls = new URL(paNotingData.get().getFileURL());
				File files = new File("filename1.html");
				FileUtils.copyURLToFile(urls, files);
				InputStream input = new FileInputStream(files);
//				String sfdt = WordProcessorHelper.load(input, FormatType.Docx);
//				responseObj.put("sfdt", sfdt.toString());
				responseObj.put("status",HttpStatus.OK);
				responseObj.put("data",paNotingDto);
				return responseObj;
			}
		}
		responseObj.put("status",HttpStatus.NOT_FOUND);
		return responseObj;
	}

	@Override
	public HashMap<String, Object> getPANoting(String id,String roleName,String grp,String token) throws Exception
	{
		responseObj.clear();
		ObjectId ids = new ObjectId(id);
		Optional<PersonalApplicationInventory> appData = personalApplicationRepository.findById(ids);
		if(appData.isPresent())
		{

			String fileName = appData.get().getPfileName();
			String subject = appData.get().getSubject();

			AmazonS3 awsClient = awsClientConfig.awsClientConfiguration(token);
			String fileNameClipped = fileName.replace("/", "");
			Optional<PersonalApplicationNotingInventory> chkFile = personalApplicationNotingRepository.findByPersonalApplicationId(id);

			if(!chkFile.isPresent())
			{
				File file = new File(this.getClass().getResource("/").getPath()+"temp2.docx");
				InputStream targetStream = new FileInputStream(file);
				ObjectMetadata metadata = new ObjectMetadata();
				metadata.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
				metadata.setContentLength(targetStream.available());
				PutObjectRequest request = new PutObjectRequest(roleName,  grp+"/"+subject+"/"+fileName+"/BM/Noting/"+fileNameClipped+".docx",targetStream,metadata);
				awsClient.putObject(request);
				URL url = awsClient.getUrl(roleName,  grp+"/"+subject+"/"+fileName+"/BM/Noting/"+fileNameClipped+".docx");
				String notingUrl = url.toString();
				PersonalApplicationNotingInventory notingPA = PersonalApplicationNotingInventory.builder().createdOn(LocalDateTime.now())
						.deptName(grp).roleName(roleName).fileName(appData.get().getPfileName()).fileURL(notingUrl).personalApplicationId(id).build();
				personalApplicationNotingRepository.save(notingPA);
			}
			responseObj.put("status",HttpStatus.OK);
			return responseObj;
		}
		responseObj.put("status",HttpStatus.NOT_FOUND);
		return responseObj;
	}

	@Override
	public HashMap<String, Object> getPAEnclosureData(String inboxId,String id,String token,String roleName,String grp)
	{
		responseObj.clear();
		ObjectId ids = new ObjectId(id);
		Optional<PersonalApplicationInventory> appData = personalApplicationRepository.findById(ids);
		List<FileAnnexureInventory> annexureData = fileAnnexureInventoryRepository.findByPersonalFileId(id);
		List<PersonalApplicationEnclosureDTO> enclosureData = new ArrayList<PersonalApplicationEnclosureDTO>();
		if(appData.isPresent() )
		{
			ObjectId idss= new ObjectId(inboxId);
			Optional<InboxInventory> inboxData = inboxInventoryRepository.findById(idss);
			AmazonS3 awsClient = awsClientConfig.awsClientConfiguration(token);
			String fileName = appData.get().getPfileName();
			String url1 = appData.get().getFileURL();
			String newUrl = url1.replaceAll(".*?\\/"+baseUrl+":"+port+"\\/.*?\\/","");
			newUrl = newUrl.replace("%20", " ");
			String newFileName = fileName.replace("/", "");
			System.out.println(newUrl);
			awsClient.copyObject(inboxData.get().getFrom(), newUrl, roleName,appData.get().getSubject()+"/"+appData.get().getPfileName()+"/BM/Enclosure/"+newFileName+".docx");
			String urls = "";
			if(awsClient.doesObjectExist(roleName,appData.get().getSubject()+"/"+appData.get().getPfileName()+"/BM/Enclosure/"+newFileName+".docx"))
			{

				URL enwUrl = awsClient.getUrl(roleName,appData.get().getSubject()+"/"+appData.get().getPfileName()+"/BM/Enclosure/"+newFileName+".docx");
				urls = enwUrl.toString();

			}
			PersonalApplicationEnclosureDTO enclosuresPAData = PersonalApplicationEnclosureDTO.builder().id(appData.get().getId().toString())
					.fileName(appData.get().getPfileName()).fileURL(urls).build();

			enclosureData.add(enclosuresPAData);
		}

		if(!annexureData.isEmpty())
		{
			int counts = 0;
			for(FileAnnexureInventory annData:annexureData) {

				counts = counts +1;
				ObjectId idss= new ObjectId(inboxId);
				Optional<InboxInventory> inboxData = inboxInventoryRepository.findById(idss);
				AmazonS3 awsClient = awsClientConfig.awsClientConfiguration(token);

				awsClient.copyObject(inboxData.get().getFrom(), "CC/PersonalApplication/Annexure/"+appData.get().getPfileName()+"/"+appData.get().getSubject()+"/"+annData.getOriginalFileName(), roleName,appData.get().getSubject()+"/"+appData.get().getPfileName()+"/BM/Enclosure/"+annData.getOriginalFileName());
				String urls = "";

				if(awsClient.doesObjectExist(roleName,appData.get().getSubject()+"/"+appData.get().getPfileName()+"/BM/Enclosure/"+annData.getOriginalFileName()))
				{
					System.out.println("First Part");
					URL enwUrl = awsClient.getUrl(roleName,appData.get().getSubject()+"/"+appData.get().getPfileName()+"/BM/Enclosure/"+annData.getOriginalFileName());
					urls = enwUrl.toString();
				}

				PersonalApplicationEnclosureDTO enclosuresData = PersonalApplicationEnclosureDTO.builder().id(annData.getId().toString())
						.fileName(annData.getOriginalFileName()).fileURL(urls).build();

				enclosureData.add(enclosuresData);
			}
		}
		if(!enclosureData.isEmpty())
		{
			responseObj.put("status",HttpStatus.OK);
			responseObj.put("data",enclosureData);
			return responseObj;
		}

		responseObj.put("status",HttpStatus.NOT_FOUND);
		return responseObj;
	}

	public String bucketPolicy(String rolename)
	{
		Policy bucket_policy = new Policy().withStatements(
				new Statement(Statement.Effect.Allow)
				.withPrincipals(Principal.AllUsers)
				.withActions(S3Actions.GetObject)
				.withResources(new Resource(
						"arn:aws:s3:::" + rolename + "/*")));
		return bucket_policy.toJson();
	}

	@Override 
	public void createPartCaseFile(String token,String grp,
			String fileName, String username, String roleName, String applicationId, String inboxId) throws IOException
	{

		Optional<PartCaseInventory> checkPartCase = partCaseInventoryRepository.findByPersonalApplicationId(applicationId);

		if(!checkPartCase.isPresent())
		{
			ObjectId ids = new ObjectId(applicationId);
			ObjectId newid = new ObjectId();
			ObjectId inboxids = new ObjectId(inboxId);
			List<PartCaseInventory> partCaseSize = partCaseInventoryRepository.findByFileNumber(fileName);
			Optional<PersonalApplicationInventory> appData = personalApplicationRepository.findById(ids);
			if(partCaseSize.isEmpty() && appData.isPresent())
			{
				PartCaseInventory newPartCase = PartCaseInventory.builder().count(1).fileNumber(fileName)
						.personalApplicationId(applicationId).roleName(roleName).userName(username).build();

				String newFile = fileName.replace(".?BM.*", "");

				PartCaseInventory newPartCases = createSupportingBM(newFile, 1, appData.get(), token, inboxId, grp, roleName, applicationId, username,newPartCase);
				partCaseInventoryRepository.save(newPartCases);
				Optional<PartCaseInventory> partFile = partCaseInventoryRepository.findByFileNumberAndPersonalApplicationIdAndRoleName(fileName, applicationId, roleName);
				Query query = new Query();
				query.addCriteria(Criteria.where("id").is(ids));
				Update update = new Update();
				update.set("isPartCase", true);
				UpdateResult result = mongoTemplate.updateMulti(query, update, PersonalApplicationInventory.class);
				Query query1 = new Query();
				query1.addCriteria(Criteria.where("id").is(inboxids));
				Update update1 = new Update();
				update1.set("partCaseFile", partFile.get().getId());
				UpdateResult result1 = mongoTemplate.updateMulti(query1, update1, InboxInventory.class);
			}
			if(!partCaseSize.isEmpty())
			{
				PartCaseInventory newPartCase = PartCaseInventory.builder().count(partCaseSize.size()+1).fileNumber(fileName)
						.personalApplicationId(applicationId).roleName(roleName).userName(username).build();
				String newFile = fileName.replace(".?BM.*", "");
				PartCaseInventory newPartCases = createSupportingBM(newFile, partCaseSize.size()+1, appData.get(), token, inboxId, grp, roleName, applicationId, username,newPartCase);
				partCaseInventoryRepository.save(newPartCases);
				Optional<PartCaseInventory> partFile = partCaseInventoryRepository.findByFileNumberAndPersonalApplicationIdAndRoleName(fileName, applicationId, roleName);
				Query query = new Query();
				query.addCriteria(Criteria.where("id").is(ids));
				Update update = new Update();
				update.set("isPartCase", true);
				UpdateResult result = mongoTemplate.updateMulti(query, update, PersonalApplicationInventory.class);
				Query query1 = new Query();
				query1.addCriteria(Criteria.where("id").is(inboxids));
				Update update1 = new Update();
				update1.set("partCaseFile", partFile.get().getId());
				UpdateResult result1 = mongoTemplate.updateMulti(query1, update1, InboxInventory.class);

			}

		}


	}

	public PartCaseInventory createSupportingBM(String newFile,int count,PersonalApplicationInventory appData,String token, String inboxId, String grp, String roleName, String applicationId, String username, PartCaseInventory newPartCase) throws IOException
	{
		if(newFile.contains("%28"))
		{
			newFile = newFile.replace("%28", "(");
		}
		if(newFile.contains("%29"))
		{
			newFile = newFile.replace("%29", ")");
		}
	
		AmazonS3 awsClient = awsClientConfig.awsClientConfiguration(token);
		//		Noting
		Optional<PersonalApplicationNotingInventory> chkFile = personalApplicationNotingRepository.findByPersonalApplicationId(applicationId);
		String fileName1 = appData.getPfileName();
		String subject = appData.getSubject();

		String fileNameClipped = fileName1.replace("/", "");

		if(!chkFile.isPresent())
		{
			File file = new File(this.getClass().getResource("/").getPath()+"temp2.docx");
			InputStream targetStream = new FileInputStream(file);
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
			metadata.setContentLength(targetStream.available());
			PutObjectRequest request = new PutObjectRequest(roleName,  newFile+"/BM/"+count+"/Noting/"+fileNameClipped+".docx",targetStream,metadata);
			awsClient.putObject(request);
			URL url = awsClient.getUrl(roleName,  newFile+"/BM/"+count+"/Noting/"+fileNameClipped+".docx");
			String notingUrl = url.toString();
			PersonalApplicationNotingInventory notingPA = PersonalApplicationNotingInventory.builder().createdOn(LocalDateTime.now())
					.deptName(grp).roleName(roleName).fileName(appData.getPfileName()).fileURL(notingUrl).personalApplicationId(applicationId).build();
			personalApplicationNotingRepository.save(notingPA);
			ObjectId newNotingId = new ObjectId();
			Optional<PersonalApplicationNotingInventory> chkFiles = personalApplicationNotingRepository.findByPersonalApplicationId(applicationId);
			PartCaseNotingFile newNotingFile = PartCaseNotingFile.builder().id(newNotingId).fileId(chkFiles.get().getId().toString()).fileName(chkFiles.get().getFileName()).fileUrl(notingUrl)
					.build();
			partCaseNotingInventoryRepository.save(newNotingFile);
			newPartCase.setNotingFileId(newNotingId.toString());
		}

		//		 Enclosure
		List<FileAnnexureInventory> annexureData = fileAnnexureInventoryRepository.findByPersonalFileId(applicationId);
		List<PersonalApplicationEnclosureDTO> enclosureData = new ArrayList<PersonalApplicationEnclosureDTO>();
		ObjectId idss= new ObjectId(inboxId);
		Optional<InboxInventory> inboxData = inboxInventoryRepository.findById(idss);
		String fileName2 = appData.getPfileName();
		String url1 = appData.getFileURL();
		String newUrl = url1.replaceAll(".*?\\/"+baseUrl+":"+port+"\\/.*?\\/","");
		newUrl = newUrl.replace("%20", " ");
		
		String newFileName = fileName2.replace("/", "");
		awsClient.copyObject(inboxData.get().getFrom(), newUrl, roleName,newFile+"/BM/1/Enclosure/"+newFileName+".docx");
		String urls = "";
		if(awsClient.doesObjectExist(roleName,newFile+"/BM/1/Enclosure/"+newFileName+".docx"))
		{
			System.out.println("First Part");
			URL enwUrl = awsClient.getUrl(roleName,newFile+"/BM/1/Enclosure/"+newFileName+".docx");
			urls = enwUrl.toString();
		}		
		PersonalApplicationEnclosureDTO enclosuresPAData = PersonalApplicationEnclosureDTO.builder().id(appData.getId().toString())
				.fileName(appData.getPfileName()).fileURL(urls).build();
		ObjectId newEnclosureId = new ObjectId();
		PartCaseEnclosureFile newEnclosureFile1 = PartCaseEnclosureFile.builder().id(newEnclosureId).fileId(appData.getId().toString()).fileName(appData.getPfileName())
				.fileUrl(urls).build();
		partCaseEnclosuresInventoryRepository.save(newEnclosureFile1);
		LinkedList<String> enclosureIds = new LinkedList<String>();
		enclosureIds.add(newEnclosureId.toString());

		if(!annexureData.isEmpty())
		{
			int counts = 0;
			for(FileAnnexureInventory annData:annexureData) {

				counts = counts +1;
				ObjectId idss1= new ObjectId(inboxId);
				Optional<InboxInventory> inboxDatas = inboxInventoryRepository.findById(idss1);
				awsClient.copyObject(inboxDatas.get().getFrom(), "CC/PersonalApplication/Annexure/"+appData.getPfileName()+"/"+appData.getSubject()+"/"+annData.getOriginalFileName(), roleName,newFile+"/BM/"+count+"/Enclosure/"+annData.getOriginalFileName());
				String urls1 = "";
				if(awsClient.doesObjectExist(roleName,newFile+"/BM/"+count+"/Enclosure/"+annData.getOriginalFileName()))
				{
					URL enwUrl = awsClient.getUrl(roleName,newFile+"/BM/"+count+"/Enclosure/"+annData.getOriginalFileName());
					urls = enwUrl.toString();
				}

				PersonalApplicationEnclosureDTO enclosuresData = PersonalApplicationEnclosureDTO.builder().id(annData.getId().toString())
						.fileName(annData.getOriginalFileName()).fileURL(urls).build();
				ObjectId newEnclosureId2 = new ObjectId();
				PartCaseEnclosureFile newEnclosureFile2 = PartCaseEnclosureFile.builder().id(newEnclosureId2).fileId(annData.getId().toString()).fileName(annData.getOriginalFileName())
						.fileUrl(urls).build();
				System.out.println(newEnclosureFile2);
				partCaseEnclosuresInventoryRepository.save(newEnclosureFile2);
				enclosureIds.add(newEnclosureId2.toString());
			}
		}
		newPartCase.setEnclosureFileId(enclosureIds);
		return newPartCase;

	}

	@Override
	public HashMap<String, Object> getPartCaseData(String id)
	{
		responseObj.clear();
		Optional<PartCaseInventory> data = partCaseInventoryRepository.findById(id);
		ObjectId newNotingId = new ObjectId(data.get().getNotingFileId());


		if(data.isPresent())
		{
			Optional<PartCaseNotingFile> notingData = partCaseNotingInventoryRepository.findById(newNotingId);
			LinkedList<String> enclIds = data.get().getEnclosureFileId();
			LinkedList<PartCaseEnclosureFile> enclosureDatas = new LinkedList<PartCaseEnclosureFile>(); 
			for(String newIds:enclIds)
			{
				ObjectId newEncosureId = new ObjectId(newIds);
				Optional<PartCaseEnclosureFile> enclosureData = partCaseEnclosuresInventoryRepository.findById(newEncosureId);
				enclosureDatas.add(enclosureData.get());

			}
			responseObj.put("status",HttpStatus.OK);
			responseObj.put("noting_file",notingData.get());
			responseObj.put("enclosure_file",enclosureDatas);
			return responseObj;
		}
		responseObj.put("status",HttpStatus.NOT_FOUND);
		return responseObj;
	}

}
