package com.office.eco.serviceImpl;

import java.awt.Color;

import org.springframework.stereotype.Service;

import com.office.eco.service.HSLColorImpl;

@Service
public class HSLColorService implements HSLColorImpl{

	@Override
	public  Color toRGB(float h, float s, float l)
	{
		return toRGB(h, s, l, 1.0f);
	}
	@Override
	public  Color toRGB(float h, float s, float l, float alpha)
	{
		if (s <0.0f || s > 100.0f)
		{
			String message = "Color parameter outside of expected range - Saturation";
			throw new IllegalArgumentException( message );
		}

		if (l <0.0f || l > 100.0f)
		{
			String message = "Color parameter outside of expected range - Luminance";
			throw new IllegalArgumentException( message );
		}

		if (alpha <0.0f || alpha > 1.0f)
		{
			String message = "Color parameter outside of expected range - Alpha";
			throw new IllegalArgumentException( message );
		}

		//  Formula needs all values between 0 - 1.

		h = h % 360.0f;
		h /= 360f;
		s /= 100f;
		l /= 100f;

		float q = 0;

		if (l < 0.5)
			q = l * (1 + s);
		else
			q = (l + s) - (s * l);

		float p = 2 * l - q;

		float r = Math.max(0, HueToRGB(p, q, h + (1.0f / 3.0f)));
		float g = Math.max(0, HueToRGB(p, q, h));
		float b = Math.max(0, HueToRGB(p, q, h - (1.0f / 3.0f)));

		r = Math.min(r, 1.0f);
		g = Math.min(g, 1.0f);
		b = Math.min(b, 1.0f);
		return new Color(r, g, b, alpha);
	}
	@Override
	public  float HueToRGB(float p, float q, float h)
	{
		if (h < 0) h += 1;

		if (h > 1 ) h -= 1;

		if (6 * h < 1)
		{
			return p + ((q - p) * 6 * h);
		}

		if (2 * h < 1 )
		{
			return  q;
		}

		if (3 * h < 2)
		{
			return p + ( (q - p) * 6 * ((2.0f / 3.0f) - h) );
		}

   		return p;
	}
	@Override
	public  String toHex(Color color) {
	    String alpha = pad(Integer.toHexString(color.getAlpha()));
	    String red = pad(Integer.toHexString(color.getRed()));
	    String green = pad(Integer.toHexString(color.getGreen()));
	    String blue = pad(Integer.toHexString(color.getBlue()));
	    String hex = red + green + blue;
	    return hex;
	}
	@Override
	public   String pad(String s) {
	    return (s.length() == 1) ? "0" + s : s;
	}
	@Override
	public String HSLToHex(float h, float s, float l) {
		return "#"+toHex(toRGB(h, s, l));
	}
}
