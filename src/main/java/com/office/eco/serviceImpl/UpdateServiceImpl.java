package com.office.eco.serviceImpl;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlCursor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities;
import org.jsoup.safety.Whitelist;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTblWidth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.pdf.BarcodeQRCode;
import com.office.eco.service.TransformHTML;
import com.office.eco.service.UpdateNoting;


@Service
public class UpdateServiceImpl implements UpdateNoting{

	@Autowired
	private TransformHTML transformHTML;
	String comment="";
	String hash=" ";
	String user=" ";
	@Override
	public ByteArrayOutputStream signStamp(String tag, String pencilColorCode, String comments, InputStream in,
			String color, String username) throws IOException {
		 System.out.println("Sign Pencil Color Code :- " + color);
		ByteArrayOutputStream bos = null;
		String customComment = "";
		user=username;
		Document doc = Jsoup.parseBodyFragment(comments);
		doc.outputSettings().prettyPrint(false);
		 comment=doc.html();
		switch (color.toUpperCase()) {
		case "BLUE":
			customComment = getSignatureBlock("#0000ff");
			break;
		case "GREEN":
			customComment =	getSignatureBlock("#008000");
			break;
		case "RED":
			customComment =	getSignatureBlock("#ff0000");
			break;
		case "BLACK":
			customComment =getSignatureBlock("#000000");
			break;
		case "PURPLE":
			customComment =getSignatureBlock("#800080");
			break;	
		default:
			customComment = getSignatureBlock(comment);
			break;
		
		}
				
		 try {
			hash=gethashofdocument(in);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		customComment = customComment.replaceAll("&nbsp;", " ");
		customComment = customComment.replaceAll("&", "&amp;");
		customComment = customComment.replaceAll(" & ", "&amp;");
		customComment = customComment.replaceAll("<br>", "<br/>");
		customComment = customComment.replaceAll("<hr>", "<hr/>");
		customComment = customComment.replaceAll("style=\"\"", "");
		
		bos = commentToDocx4J(tag, customComment, in,color);
		
		if (bos == null) {
			System.out.println("*********************************** Plain Comment ******************************");
			String plainComment = null;
			boolean valid = Jsoup.isValid(comments, Whitelist.basic());

			if (!valid) {
				Document dirtyDoc = Jsoup.parse(comments);
				dirtyDoc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
				dirtyDoc.outputSettings().charset("UTF-8");
				plainComment = dirtyDoc.body().text();

				System.out.println("Plain Text :- " + plainComment);

				in.close();
				bos.toByteArray();
				return bos;
			} else {
				System.out.println("************************* Without Docx4j sign ********************* ");

				Document dirtyDoc = Jsoup.parse(comments);
				dirtyDoc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
				dirtyDoc.outputSettings().charset("UTF-8");
				plainComment = dirtyDoc.body().text();

				return bos;
			}

		} else {
			System.out.println("BOS NOT NULL");
			in.close();
			return bos;
		}
	}
	private String getSignatureBlock(String color) {
		String customComment;
		if(color.equals("#0000ff")){
		customComment = "<div ><font face=\"arial\" margin-right=\"10px\" color=\"#0000ff\" size=\"0\">" +  comment + "</font></div>";
		customComment = customComment.replace("rgb(0, 0, 0);", "#0000ff;");

		}
		else if(color.equals("#008000")){
			customComment = "<div ><font face=\"arial\" margin-right=\"10px\" color=\"#008000\" size=\"0\">" +  comment + "</font></div>";
			customComment = customComment.replace("rgb(0, 0, 0);", "#008000;");
	
			}
		else if(color.equals("#ff0000")){
			customComment = "<div ><font face=\"arial\" margin-right=\"10px\" color=\"#ff0000\" size=\"0\">" +  comment + "</font></div>";
			customComment = customComment.replace("rgb(0, 0, 0);", "#ff0000;");
		
			}
		else if(color.equals("#000000")){
			customComment = "<div ><font face=\"arial\" margin-right=\"10px\" color=\"#000000\" size=\"0\">" +  comment + "</font></div>";
			customComment = customComment.replace("rgb(0, 0, 0);", "#000000;");
			
			}
		else if(color.equals("#800080")){
			customComment = "<div ><font face=\"arial\" margin-right=\"10px\" color=\"#800080\" size=\"0\">" +  comment + "</font></div>";
			customComment = customComment.replace("rgb(0, 0, 0);", "#800080;");
		
			}
		else {
			customComment = "<div><font face=\"arial\" color=\"#000000\" size=\"0\">" + comment + "</font></div>";
		}
		return customComment;
			
	}
	private ByteArrayOutputStream commentToDocx4J(String tag, String comments, InputStream is,String colour) {
		try {
			ByteArrayOutputStream bos = transformHTML.transformParsing(comments);

			byte[] bytes = bos.toByteArray();
			InputStream inputStream = new ByteArrayInputStream(bytes);
			ByteArrayOutputStream baos = commentToPOI(tag, inputStream, is,colour);
			bos.close();
			inputStream.close();
			return baos;

		} catch (Exception e) {
			System.out.println("************************* Docx4j Exception *************************");
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			String sStackTrace = sw.toString(); // stack trace as a string
			System.out.println("Docx4j Stack Trace : " + sStackTrace);
			return null;
		}
	}
	private String gethashofdocument(InputStream in) throws NoSuchAlgorithmException {
		 byte bytes1[] = in.toString().getBytes();
		  ByteArrayInputStream byteArrayInputStream = new 
		ByteArrayInputStream(bytes1);
		  int size = byteArrayInputStream.available();
		    char[] theChars = new char[size];
		    byte[] bytes = new byte[size];

		    byteArrayInputStream.read(bytes, 0, size);
		    for (int i = 0; i < size;)
		      theChars[i] = (char) (bytes[i++] & 0xff);
		    String ss=new String(theChars);
		    MessageDigest md = MessageDigest.getInstance("SHA-256");
		    byte[] hash=md.digest(ss.getBytes(StandardCharsets.UTF_8)); 
		    BigInteger number = new BigInteger(1, hash); 
	        StringBuilder hexString = new StringBuilder(number.toString(16)); 
	        while (hexString.length() < 32) 
	        { 
	            hexString.insert(0, '0'); 
	        }
	        
		return hexString.toString();
	}
	private ByteArrayOutputStream commentToPOI(String tag, InputStream comment, InputStream srcfile,String colour) {

		System.out.println("In comment to POI");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		int twipsPerInch = 1440;
		XmlCursor cursor = null;
		XWPFParagraph paragraph = null;
		XWPFRun run = null;
		String kept = tag.substring(0, tag.lastIndexOf("O") - 1);
		String remainder = tag.substring(tag.lastIndexOf("O") - 1, tag.length());

		try {

			ByteArrayInputStream stream = qr_code(tag,colour);
			System.out.println("Qr Code Genrated");
			XWPFDocument doc = new XWPFDocument(OPCPackage.open(comment));
			List<IBodyElement> belements = doc.getBodyElements();

			OPCPackage src1Package = OPCPackage.open(srcfile);
			System.out.println("Documentum Created");
			XWPFDocument src1Document = new XWPFDocument(src1Package);
			System.out.println("searching sign block");


			for (XWPFParagraph par : src1Document.getParagraphs()) {
				int pos = 0;
				List<XWPFRun> runs = par.getRuns();
				if (runs != null) {
					for (XWPFRun r : runs) {
						System.out.println("finding sign");
						String text = r.getText(0);
						System.out.println("text "+text);
						if (text!= null && (text.contains("<Signature Block>") || text.contains("<Signature>")))
						{
							System.out.println("text "+text);
							par.removeRun(pos);
							cursor = par.getCTP().newCursor();
							System.out.println("got signature block");
							break;
						}
						else {
							pos++;
						}

					}
				}
			}	

						XWPFTable table = null;
			if (cursor == null) {
				System.out.println((Object)"Placing sign table and comments at null cursor");
				XWPFParagraph p = null;
				int np = src1Document.getParagraphs().size();
				int nt = src1Document.getTables().size();
				System.out.println((Object)("belement size" + belements.size()));
				XWPFParagraph newBody1 = null;
				for (int i = 0; i < belements.size(); ++i) {
					++np;
					XWPFParagraph p2 = (XWPFParagraph)belements.get(i);
					newBody1 = src1Document.createParagraph();
					int pos2 = src1Document.getPosOfParagraph(newBody1);
					System.out.println((Object)("Text to set " + p2.getText()));
					src1Document.setParagraph(p2, pos2 - nt);
				}
				System.out.println((Object)"comments at cursor");
				cursor = newBody1.getCTP().newCursor();
				System.out.println((Object)("CURSOR VAL" + cursor.getTextValue()));
				System.out.println((Object)("ff" + cursor.getTextValue()));
				cursor.toEndToken();
				while (cursor.toNextToken() != XmlCursor.TokenType.START) {}
				table = src1Document.insertNewTbl(cursor);
			}
			else {
				System.out.println((Object)"Placing sign table and comments at cursor Signblock");
				XWPFParagraph newBody2=null;
				int np2 = src1Document.getParagraphs().size();
				int nt2 = src1Document.getTables().size();
				for (int i = 0; i < belements.size(); ++i) {
					++np2;
					XWPFParagraph p2 = (XWPFParagraph)belements.get(i);
					newBody2 = src1Document.insertNewParagraph(cursor);
					cursor.toNextToken();
					int pos2 = src1Document.getPosOfParagraph(newBody2);
					System.out.println((Object)("Text to set " + p2.getText()));
					src1Document.setParagraph(p2, pos2 - nt2);
				}
			
				System.out.println((Object)"comments at cursor");
				cursor = newBody2.getCTP().newCursor();
				System.out.println((Object)("CURSOR VAL" + cursor.getTextValue()));
				cursor.toEndToken();
				while (cursor.toNextToken() != XmlCursor.TokenType.START) {}
				table = src1Document.insertNewTbl(cursor);
					System.out.println("table alignment set to right");
			}
			System.out.println("Table content in set...with NO WIDTH MENTIONED");
			table.getCTTbl().getTblPr().unsetTblBorders();
				table.setWidth(7 * twipsPerInch);
			System.out.println("table width "+table.getWidth());
			table.getCTTbl().addNewTblGrid().addNewGridCol().setW(BigInteger.valueOf(8 * twipsPerInch));
			table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(2 * twipsPerInch));
			XWPFTableRow tableRow = table.getRow(0);
			XWPFTableCell cell = tableRow.getCell(0);
			CTTblWidth tblWidth = cell.getCTTc().addNewTcPr().addNewTcW();
			tblWidth.setW(BigInteger.valueOf(8 * twipsPerInch));
			tblWidth.setType(STTblWidth.DXA);
			paragraph = cell.getParagraphArray(0);
			System.out.println("Setting text color");
			if (paragraph == null)
				paragraph = cell.addParagraph();
			paragraph.setAlignment(ParagraphAlignment.RIGHT);
			run = paragraph.createRun();
			run.setFontSize(12);
			run.setFontFamily("Arial");
			switch (colour.toUpperCase()) {
			case "BLUE":
				run.setColor("0000ff");
				break;
			case "PURPLE":
				run.setColor("800080");
				break;	
			case "GREEN":
				run.setColor("008000");
				break;
			case "RED":
				run.setColor("ff0000");
				break;
			case "BLACK":
				run.setColor("000000");
				break;
			default:
				run.setColor("000000");
				break;
			}
			run.addBreak();
			run.setText(kept);
			run.addBreak();

			XWPFRun run2 = paragraph.createRun();
			run2.setFontFamily("Arial");
			run2.setColor(run.getColor());
			run2.setItalic(true);
			run2.setFontSize(12);
			run2.setText(remainder);
			run2.addBreak();
			run2.setText("using Login ID authentication through eOffice");
			System.out.println("commentToPOI single add break");
			cell = tableRow.addNewTableCell();
			cell.removeParagraph(0);
			paragraph = cell.getParagraphArray(0);
			if (paragraph == null)
				paragraph = cell.addParagraph();

			XWPFRun run3 = paragraph.createRun();
			run3.addBreak();
			run3.addPicture(stream, XWPFDocument.PICTURE_TYPE_JPEG, "imgFile", Units.toEMU(55), Units.toEMU(55));
			src1Document.write(baos);
			doc.close();
			src1Document.close();
			System.out.println("documents closed");
			return baos;

		} catch (Exception e) {
			System.out.println("************************ POI Exception ************************");
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			String sStackTrace = sw.toString(); // stack trace as a string
			System.out.println("POI Stack Trace : " + sStackTrace);

		} 
		return null;
	}
	private ByteArrayInputStream qr_code(String text,String colour) throws IOException {
		System.out.println("QRCODE Text-->" + text);
		String myString = text+" "+hash+" "+user;
		//String myString = text;
		BarcodeQRCode qrcode = new BarcodeQRCode(myString.trim(), 1, 1, null);
		java.awt.Image img;
		System.out.println("purple added");
		switch (colour.toUpperCase()) {
		case "BLUE":
			img = qrcode.createAwtImage(Color.BLUE, Color.WHITE);
			break;
		case "GREEN":
			img = qrcode.createAwtImage(Color.GREEN, Color.WHITE);
			break;
		case "RED":
			img = qrcode.createAwtImage(Color.RED, Color.WHITE);
			break;
		case "BLACK":
			img = qrcode.createAwtImage(Color.BLACK, Color.WHITE);
			break;
		case "PURPLE":
			img = qrcode.createAwtImage(Color.decode("#800080"), Color.WHITE);
			break;	
		default:
			img = qrcode.createAwtImage(Color.BLACK, Color.WHITE);
			break;
		}

		BufferedImage outImage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);
		outImage.getGraphics().drawImage(img, 0, 0, null);
		ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
		ImageIO.write(outImage, "png", bytesOut);
		bytesOut.flush();
		byte[] pngImageData = bytesOut.toByteArray();
		ByteArrayInputStream stream = new ByteArrayInputStream(pngImageData);
		System.out.println("Stream of QR CODE:-->" + stream);
		return stream;
	}

	
	
}
