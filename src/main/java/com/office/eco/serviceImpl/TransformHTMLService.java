package com.office.eco.serviceImpl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.docx4j.convert.in.xhtml.XHTMLImporterImpl;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.office.eco.service.HSLColorImpl;
import com.office.eco.service.TransformHTML;
import com.office.eco.service.TransformHTMLSupport;

@Service
public class TransformHTMLService implements TransformHTML{


	WordprocessingMLPackage docXTemplate = null;
	XHTMLImporterImpl xHTMLImporter = null;

	public ByteArrayOutputStream transformParsing(String comments) throws Exception {
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		docXTemplate = WordprocessingMLPackage.createPackage();
		xHTMLImporter = new XHTMLImporterImpl(docXTemplate);
		
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		//HTMLParser htmlHandler = new TransformHTMLService().new HTMLParser();
		TransformHTMLSupportImpl htmlHandler = new TransformHTMLSupportImpl();
		InputStream in = new ByteArrayInputStream(comments.getBytes(StandardCharsets.UTF_8));
		saxParser.parse(in, htmlHandler);

		// Convert to DocX
		String htmlDOCX = htmlHandler.getXMLForDOCX();
		

		MainDocumentPart documentPart = docXTemplate.getMainDocumentPart();
		documentPart.getContent().addAll(xHTMLImporter.convert(htmlDOCX, null));

		docXTemplate.save(bos);
		in.close();
		return bos;
	}

	}



