package com.office.eco.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.office.eco.service.HSLColorImpl;
import com.office.eco.service.TransformHTMLSupport;

@Service
public class TransformHTMLSupportImpl extends DefaultHandler implements TransformHTMLSupport {

	int bDiv = 0;
	boolean bFont = false;
	boolean bSpan = false;
	boolean bB = false;
	boolean bI = false;
	boolean bU = false;
	boolean bOL = false;
	boolean bLI = false;
	boolean bUL = false;
	boolean bStrong = false;
	boolean bEm = false;
	boolean bp = false;
	boolean bfigure = false;
	boolean btable = false;
	boolean btbody = false;
	boolean btr = false;
	boolean btd = false;
	boolean bth = false;
	boolean bthead = false;
	boolean bh1 = false;
	boolean bh2 = false;
	boolean bh3 = false;
	String xmlForDOCX = "";

	@Autowired
	private HSLColorImpl HSLColor;
	/**
	 * Get the Transformed HTML that can be feed to the DOCX converter.
	 */
	@Override
	public String getXMLForDOCX() {
		return xmlForDOCX;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException {
		// log.warn("SE qName: " + qName + " Attributes Size: " +
		// attributes.getLength());

		if (qName.equalsIgnoreCase("div")) {
			bDiv++;
			System.out.println("DIV Depth: " + bDiv);
			xmlForDOCX += "<div " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("font")) {
			bFont = true;
			xmlForDOCX += "<font " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("span")) {
			bSpan = true;
			xmlForDOCX += "<span " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("b")) {
			bB = true;
			xmlForDOCX += "<b " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("i")) {
			bI = true;
			xmlForDOCX += "<i>";
		} else if (qName.equalsIgnoreCase("u")) {
			bU = true;
			xmlForDOCX += "<u>";
		} else if (qName.equalsIgnoreCase("ul")) {
			bUL = true;
			xmlForDOCX += "<ul " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("ol")) {
			bOL = true;
			xmlForDOCX += "<ol " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("li")) {
			bLI = true;
			xmlForDOCX += "<li " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("strong")) {
			bStrong = true;
			xmlForDOCX += "<strong" + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("em")) {
			bEm = true;
			xmlForDOCX += "<em" + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("p")) { /* new added for <p> here 2/5/2020 */
			bp = true;
			xmlForDOCX += "<p " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("figure")) {
			bfigure = true;
			xmlForDOCX += "<figure " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("table")) {
			btable = true;
			xmlForDOCX += "<table " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("tbody")) {
			btbody = true;
			xmlForDOCX += "<tbody " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("tr")) {
			btr = true;
			xmlForDOCX += "<tr " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("td")) {
			btd = true;
			xmlForDOCX += "<td " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("th")) {
			bth = true;
			xmlForDOCX += "<th " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("thead")) {
			bthead = true;
			xmlForDOCX += "<thead " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("h1")) {
			bh1 = true;
			xmlForDOCX += "<h1 " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("h2")) {
			bh2 = true;
			xmlForDOCX += "<h2 " + getStyle(attributes) + ">";
		} else if (qName.equalsIgnoreCase("h3")) {
			bh3 = true;
			xmlForDOCX += "<h3 " + getStyle(attributes) + ">";
		}
	}

	private String getStyle(Attributes attributes) {
		String style = "style=\"";

		/*
		 * for (int i = 0; i < attributes.getLength(); i++) {
		 * log.warn("IN LOOP Attributes IN ::" + attributes.getQName(i)); }
		 * 
		 * log.warn("CHECK STYLE Exists? " + attributes.getIndex("style"));
		 */

		if (attributes.getLength() > 0) {
			/* =============== style Attribute =============== */
			int indStyle = attributes.getIndex("style");
			if (indStyle >= 0) {
				String qNameKey = attributes.getQName(indStyle);
				String attrValue = attributes.getValue(indStyle);
				// log.warn("STYLE qNameKey:: " + qNameKey + " attrValue: " + attrValue);
				if (attrValue.contains("fontfamily")) {
					style += attrValue.replace("fontfamily", "font-family") + ";";
				} else {
					style += attrValue + ";";
				}
			}

			/* =============== color Attribute =============== */
			int indColor = attributes.getIndex("color");
			if (indColor >= 0) {
				String qNameKey = attributes.getQName(indColor);
				String attrValue = attributes.getValue(indColor);
				// log.warn("COLOR qNameKey::" + qNameKey + " arrrValue: " + attrValue);
				if (attrValue.contains("color")) {
					style += qNameKey + ":" + attrValue + ";";
				} else {
					style += qNameKey + ":" + attrValue + ";";
				}
			}

			/* =============== face Attribute =============== */
			int indFace = attributes.getIndex("face");
			if (indFace >= 0) {
				String qNameKey = attributes.getQName(indFace);
				String attrValue = attributes.getValue(indFace);
				// log.warn("FACE qNameKey::" + qNameKey + " arrrValue: " + attrValue);
				style += "font-family:" + attrValue + ";";
			}

			/* =============== size Attribute =============== */
			int indSize = attributes.getIndex("class");
			// System.out.println(indSize);
			if (indSize >= 0) {
				String attrValue = attributes.getValue(indSize);
				int size = 12;
				size = size + (attrValue.equalsIgnoreCase("text-huge") ? 4
						: attrValue.equalsIgnoreCase("text-big") ? 2
								: attrValue.equalsIgnoreCase("text-small") ? -2 : -4);
				style += "font-size:" + size + "pt;";
			} else {
				if (!style.contains("font-size"))
					style += "font-size:12pt;";
			}

		} else {
			return "";
		}

		style += "\"";
		while (style.contains("hsl(")) {
			String hsl = style.substring(style.indexOf("hsl("), style.indexOf(")") + 1);
			System.out.println(hsl);
			float h = Float.parseFloat(hsl.substring(4, hsl.indexOf(",")));
			float s = Float.parseFloat(hsl.substring(hsl.indexOf(",") + 1, hsl.indexOf("%")));
			float l = Float.parseFloat(hsl.substring(hsl.indexOf("%") + 2, hsl.indexOf(")") - 1));
			// System.out.println(h+" "+s+" "+l);
			String hex = HSLColor.HSLToHex(h, s, l);
			System.out.println(hex);
			style = style.replace(hsl, hex);
			System.out.println(style);
		}
		// log.warn("STYLE:-->" + style);
		// System.out.println(style);
		return style;

	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {

		// log.warn("EE qName: " + qName);

		if (qName.equalsIgnoreCase("div")) {
			bDiv++;
			// log.warn("DIV Depth: " + bDiv);
			xmlForDOCX += "</div>";
		} else if (qName.equalsIgnoreCase("font")) {
			bFont = true;
			xmlForDOCX += "</font>";
		} else if (qName.equalsIgnoreCase("span")) {
			bSpan = true;
			xmlForDOCX += "</span>";
		} else if (qName.equalsIgnoreCase("b")) {
			bB = true;
			xmlForDOCX += "</b>";
		} else if (qName.equalsIgnoreCase("i")) {
			bI = true;
			xmlForDOCX += "</i>";
		} else if (qName.equalsIgnoreCase("u")) {
			bU = true;
			xmlForDOCX += "</u>";
		} else if (qName.equalsIgnoreCase("ul")) {
			bUL = true;
			xmlForDOCX += "</ul>";
		} else if (qName.equalsIgnoreCase("ol")) {
			bOL = true;
			xmlForDOCX += "</ol>";
		} else if (qName.equalsIgnoreCase("li")) {
			bLI = true;
			xmlForDOCX += "</li>";
		} else if (qName.equalsIgnoreCase("strong")) {
			bStrong = true;
			xmlForDOCX += "</strong>";
		} else if (qName.equalsIgnoreCase("p")) {
			bp = true;
			xmlForDOCX += "</p>";
		} else if (qName.equalsIgnoreCase("em")) {
			bEm = true;
			xmlForDOCX += "</em>";
		} else if (qName.equalsIgnoreCase("figure")) {
			bfigure = true;
			xmlForDOCX += "</figure>";
		} else if (qName.equalsIgnoreCase("table")) {
			btable = true;
			xmlForDOCX += "</table>";
		} else if (qName.equalsIgnoreCase("tbody")) {
			btbody = true;
			xmlForDOCX += "</tbody>";
		} else if (qName.equalsIgnoreCase("tr")) {
			btr = true;
			xmlForDOCX += "</tr>";
		} else if (qName.equalsIgnoreCase("td")) {
			btd = true;
			xmlForDOCX += "</td>";
		} else if (qName.equalsIgnoreCase("th")) {
			bth = true;
			xmlForDOCX += "</th>";
		} else if (qName.equalsIgnoreCase("thead")) {
			bthead = true;
			xmlForDOCX += "</thead>";
		} else if (qName.equalsIgnoreCase("h1")) {
			bh1 = true;
			xmlForDOCX += "</h1>";
		} else if (qName.equalsIgnoreCase("h2")) {
			bh2 = true;
			xmlForDOCX += "</h2>";
		} else if (qName.equalsIgnoreCase("h3")) {
			bh3 = true;
			xmlForDOCX += "</h3>";
		}
		System.out.println("NEW HTML SO FAR: " + xmlForDOCX);

	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		// docXTemplate.getMainDocumentPart().getContent().addAll(xHTMLImporter.convert(tidyHTML,
		// null));
		// log.warn("CC ch[]: " + new String(ch, start, length) +
		// " start: " + start + " lenght: " + length);
		xmlForDOCX += new String(ch, start, length);
	}
}
