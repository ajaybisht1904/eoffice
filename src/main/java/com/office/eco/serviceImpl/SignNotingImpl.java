package com.office.eco.serviceImpl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.office.eco.dto.OutputBean1;
import com.office.eco.service.SignNotingService;
import com.office.eco.service.SignService;

@Service
public class SignNotingImpl implements SignNotingService{

	@Autowired
	private SignService signService;
	
	@Override
public OutputBean1 signNoting(String comments,String signTitle,InputStream file,String tag,String pencilColorCode,String username,String dep_desc,String color) throws IOException {
		OutputBean1 out = new OutputBean1();
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		Date date = new Date();
		
		if (pencilColorCode == null || pencilColorCode == "") {
			pencilColorCode = "blue";
		}
		
		if(dep_desc!=null) {
			String tags = signTitle + " " +  dep_desc + " On "
					+ (String) dateFormat.format(date);
		
		
		if (comments.equals(""))
			comments = "";
		
		System.out.println(comments+signTitle+file+tag+pencilColorCode+username+dep_desc+color);
		ByteArrayOutputStream b = signService.signdocs(comments,file,tags,pencilColorCode,username,dep_desc,color);
	  
		out.setResultsuccess("Sign Successful");
		out.setResult(b.toByteArray());
		return out;
	  
		}
		else {
			return null;
		}
		
		
	}
}
