package com.office.eco.camunda.externalTask;

import org.camunda.bpm.client.ExternalTaskClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:24:49 PM
 *
 * office
 */
@Service
public class CamundaUtilsImpl implements CamundaUtils {

	@Value("${camunda.rest-url}")
	private String baseUrl;

	@Override
	public ExternalTaskClient createExternalTask() {
		ExternalTaskClient client = ExternalTaskClient.create().baseUrl(baseUrl).asyncResponseTimeout(1000).build();
		return client;
	}
}
