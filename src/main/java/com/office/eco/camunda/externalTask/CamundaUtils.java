package com.office.eco.camunda.externalTask;

import org.camunda.bpm.client.ExternalTaskClient;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:25:12 PM
 *
 * office
 */
public interface CamundaUtils {

	public ExternalTaskClient createExternalTask();
}
