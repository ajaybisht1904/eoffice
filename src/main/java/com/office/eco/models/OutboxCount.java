package com.office.eco.models;

import com.office.eco.models.InboxCount.InboxCountBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OutboxCount {

	private String outbox;

	private long total;
}
