package com.office.eco.models;

import java.time.LocalDateTime;
import java.util.LinkedList;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "part_case_inventory")
public class PartCaseInventory {
	
	@Id
	private String id;
	@Field
	private String roleName;
	
	@Field
	private String userName;
	
	@Field
	private String fileNumber;
	
	@Field
	private String notingFileId;
	
	@Field
	private int count;
	
	@Field
	private String personalApplicationId;
	
	@Field
	private LinkedList<String> enclosureFileId;
	
	@Field
	private LocalDateTime createdOn;

}
