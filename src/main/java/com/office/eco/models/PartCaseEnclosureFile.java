package com.office.eco.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "part_case_enclosure_inventory")
public class PartCaseEnclosureFile {
	@Id
	private ObjectId id;
	@Field
	private String fileId;
	@Field
	private String fileName;
	@Field
	private String fileUrl;
	@Field
	private String signedId;
	@Field
	private String annotId;
}