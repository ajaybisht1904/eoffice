package com.office.eco.models;

import java.time.LocalDateTime;

import javax.persistence.Entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:27:18 PM
 *
 * office
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "file_annexure_inventory")
public class FileAnnexureInventory {

	@Field
	private String by;

	@Field
	private String fileName;

	@Field
	private LocalDateTime uploadingDate;

	@Id
	@Field
	private ObjectId id;

	@Field
	private String status;

	@Field
	private String subject;
	
	@Field
	private String personalFileId;
	
	@Field
	private String annexureFileURL;
	
	@Field
	private long contentLength;

	@Field
	private String contentId;
	
	@Field
	private String mimeType;
	
	@Field
	private String originalFileName;
	
	@Field
	private String versionNumber;
	
	@Field
	private String signedId;
	@Field
	private String annotationId;
}
