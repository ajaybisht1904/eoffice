package com.office.eco.models;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:27:51 PM
 *
 * office
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "outbox_inventory")
public class OutboxInventory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7559260777517362213L;

	@Field
	private String comments;

	@Field
	private LocalDateTime dateSent;

	@Field
	private String personalApplicationInventoryId;

	@Id
	@Field
	private String id;

	@Field
	private String roleName;

	@Field
	private String type;
	
	@Field
	private Boolean isNewEntry;

	@Field
	private Integer status;

	@Field
	private List<String> to;
	
	@Field
	private String trackId;
	
	@Field
	private String fileInventoryId;

	@Field
	private String partCaseFileId;

}
