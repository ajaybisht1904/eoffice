package com.office.eco.models;

import com.office.eco.models.OutboxCount.OutboxCountBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PfileCount {

	
	private String pfile;

	private long total;
}
