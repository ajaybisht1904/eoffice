package com.office.eco.models;

import java.time.LocalDateTime;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "personal_file_inventory")
public class PersonalFileInventory {

	@Id
	@Field
	private ObjectId id;

	@Field
	private String subject;

	@Field
	private String fileName;
	
	@Field
	private long fileCount;
	
	@Field
	private String status;

	@Field
	private LocalDateTime createdOn;

	@Field
	private String pFileId;

	@Field
	private String roleName;
	
	@Field
	private String username;

}
