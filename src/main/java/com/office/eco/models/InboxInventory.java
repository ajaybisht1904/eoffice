package com.office.eco.models;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:27:39 PM
 *
 * office
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "inbox_inventory")
public class InboxInventory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5741150998988005220L;

	@Field
	private String trackId;

	@Field
	private String personalApplicationInventoryId;

	@Field
	private String from;

	@Id
	@Field
	private ObjectId id;

	@Field
	private String roleName;

	@Field
	private String type;
	
	@Field
	private String personalUrl;
	
	@Field
	private String partCaseFile;

	@Field
	private String fileNumber;
	
	@Field
	private Boolean isNewEntry;

	@Field
	private LocalDateTime recieveDate;
	
	@Field
	private LocalDateTime sentDate;

	@Field
	private Integer status;

	@Field
	private List<String> userList;

	@Field
	private String fileInventoryId;

	
}
