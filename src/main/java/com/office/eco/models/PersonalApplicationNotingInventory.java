package com.office.eco.models;

import java.time.LocalDateTime;



import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "personal_application_noting_inventory")
public class PersonalApplicationNotingInventory {

	@Id
	@Field
	private ObjectId id;

	@Field
	private String subject;

	@Field
	private String status;
	
	@Field
	private LocalDateTime createdOn;
	
	@Field
	private String fileName;
	
	@Field
	private String fileURL;

	private String userName;
	@Field
	private String roleName;
	@Field
	private String deptName;
	@Field
	private String personalApplicationId;

	@Field
	private String signedId;
	@Field
	private String annotationId;
}