package com.office.eco.models;

import java.time.LocalDateTime;



import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "personal_application_inventory")
public class PersonalApplicationInventory {

	@Id
	@Field
	private ObjectId id;

	@Field
	private String subject;
	@Field
	private Integer vol;
	@Field
	private String rank;
	@Field
	private boolean isPartCase;
	@Field
	private String decoration;
	@Field
	private String status;
	@Field
	private LocalDateTime createdOn;
	@Field
	private String signedId;
	
	@Field
	private String pFileId;
	@Field
	private String pfileName;
	@Field
	private String fileURL;

	@Field
	private String type;
	@Field
	private String userName;
	@Field
	private String roleName;
	@Field
	private String deptName;
	@Field
	private String notingUrl;
	@Field
	private String annotationId;
	

}