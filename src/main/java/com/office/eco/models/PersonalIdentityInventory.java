package com.office.eco.models;

import java.io.Serializable;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:27:34 PM
 *
 * office
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "personal_identity_inventory")
public class PersonalIdentityInventory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4615429319881575087L;
	@Field
	private String name;

	@Id
	@Field
	private ObjectId id;

	@Field
	private String username;

	@Field
	private String designation;
	
	@Field
	private String email;
	
	@Field
	private String address1;
	
	@Field
	private String address2;
	
	@Field
	private String address3;
}
