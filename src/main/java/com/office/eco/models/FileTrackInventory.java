package com.office.eco.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "file_track_inventory")
public class FileTrackInventory {

	@Field
	private String fileStatus;

	@Id
	@Field
	private ObjectId trackId;

	@Field
	private String fileId;
	
	@Field
	private String from;
	
	@Field
	private String to;
	
}
