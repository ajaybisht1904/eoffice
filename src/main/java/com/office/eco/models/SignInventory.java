package com.office.eco.models;

import java.time.LocalDateTime;



import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "signature_inventory")
public class SignInventory {

	@Id
	@Field
	private ObjectId id;

	@Field
	private String signedBy;
	@Field
	private LocalDateTime signedOn;
}