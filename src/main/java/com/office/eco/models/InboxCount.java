package com.office.eco.models;

import java.time.LocalDateTime;
import java.util.List;

import org.bson.types.ObjectId;

import com.office.eco.models.InboxInventory.InboxInventoryBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InboxCount {

	private String inbox;

	private long total;

}
