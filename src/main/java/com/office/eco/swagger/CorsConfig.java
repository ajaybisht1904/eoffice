package com.office.eco.swagger;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:29:31 PM
 *
 * office
 */
@Configuration
@Profile("development")
public class CorsConfig implements WebMvcConfigurer {

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/api/**").allowedOrigins("*").allowedMethods("*")
				.exposedHeaders("Authorization");
	}
}