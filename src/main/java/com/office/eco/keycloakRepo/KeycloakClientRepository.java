package com.office.eco.keycloakRepo;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.office.eco.keycloakModels.KeycloakClient;



@Repository
public interface KeycloakClientRepository extends JpaRepository<KeycloakClient, String> {

		public Optional<KeycloakClient> findByRealmIdAndClientId(String realmId, String clientId);
}
