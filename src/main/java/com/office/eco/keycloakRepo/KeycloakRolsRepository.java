package com.office.eco.keycloakRepo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.office.eco.keycloakModels.KeycloakRoles;


@Repository
public interface KeycloakRolsRepository extends JpaRepository<KeycloakRoles, String> {

		public List<KeycloakRoles> findByRealmIdAndClientId(String realmId, String clientId);
		
		public Optional<KeycloakRoles> findByRoleName(String roleName);
		public Optional<KeycloakRoles> findByRoleId(String roleId);
}
