package com.office.eco.keycloakRepo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.office.eco.keycloakModels.KeycloakUsersGroup;


//@Repository
public interface KeycloakUserGroupMembershipRepository extends JpaRepository<KeycloakUsersGroup, String> {

	public Optional<KeycloakUsersGroup> findByUserIdAndGroupId(String userId, String grpId);

	@Query("SELECT u.groupId FROM KeycloakUsersGroup u WHERE u.userId = :userId")
	public String findKeycloakUsersGroupByUserId(@Param("userId") String userId);

}
