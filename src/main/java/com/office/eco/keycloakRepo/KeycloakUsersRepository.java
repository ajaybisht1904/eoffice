package com.office.eco.keycloakRepo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.office.eco.keycloakModels.KeycloakUsers;


@Repository
public interface KeycloakUsersRepository extends JpaRepository<KeycloakUsers, String> {

	public Optional<KeycloakUsers> findByUserName(String userName);
}
