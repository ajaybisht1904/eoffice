package com.office.eco.keycloakRepo;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.office.eco.keycloakModels.KeycloakGroups;


@Repository
public interface KeycloakGroupRepository extends JpaRepository<KeycloakGroups, Integer> {

	public Optional<KeycloakGroups> findBygrpName(String name);
	public Optional<KeycloakGroups> findByGrpId(String Id);
}
