package com.office.eco.keycloakRepo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.office.eco.keycloakModels.KeycloakGroupRoleMapping;


@Repository
public interface KeycloakGroupRoleMappingRepository extends JpaRepository<KeycloakGroupRoleMapping, String> {

	public List<KeycloakGroupRoleMapping> findByGroupId(String grpId);
	
	public Optional<KeycloakGroupRoleMapping> findByRoleId(String roleId);
	
	public Optional<KeycloakGroupRoleMapping> findByRoleIdAndGroupId(String roleId, String groupId);
}
