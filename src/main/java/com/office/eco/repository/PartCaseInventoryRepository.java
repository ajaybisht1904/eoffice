package com.office.eco.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.office.eco.models.PartCaseInventory;

public interface PartCaseInventoryRepository extends MongoRepository<PartCaseInventory, String>{

	List<PartCaseInventory> findByRoleName(String roleName);
	
	Optional<PartCaseInventory> findByPersonalApplicationId(String id);
	
	Optional<PartCaseInventory> findByFileNumberAndPersonalApplicationIdAndRoleName(String fileName,String id,String role);
	
	Optional<PartCaseInventory> findByFileNumberAndPersonalApplicationId(String fileName,String id);
	
	List<PartCaseInventory> findByFileNumber(String id);

}
