package com.office.eco.repository;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.office.eco.models.PersonalApplicationNotingInventory;


/**
* @author Aman Gupta
*
* Date : Nov 5, 2020 Time : 12:28:21 PM
*
* office
*/
@Repository
public interface PersonalApplicationNotingRepository extends MongoRepository<PersonalApplicationNotingInventory, ObjectId> {

	public Optional<PersonalApplicationNotingInventory> findByPersonalApplicationIdAndFileName(String id,String fileName);
	public Optional<PersonalApplicationNotingInventory> findByPersonalApplicationId(String id);
}

