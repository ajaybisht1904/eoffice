package com.office.eco.repository;
import java.util.List;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.office.eco.models.PersonalApplicationInventory;
import com.office.eco.models.PersonalFileInventory;


/**
* @author Aman Gupta
*
* Date : Nov 5, 2020 Time : 12:28:21 PM
*
* office
*/
@Repository
public interface PersonalApplicationRepository extends MongoRepository<PersonalApplicationInventory, ObjectId> {

 public Page<PersonalApplicationInventory> findByStatusAndRoleNameOrderByCreatedOnDesc(String status,String roleName,Pageable pages);

public List<PersonalApplicationInventory> findBypfileName(String pfileName);
}
