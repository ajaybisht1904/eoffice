package com.office.eco.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:28:09 PM
 *
 * office
 */
@Repository
public interface FileTrackInventoryRepository extends MongoRepository<com.office.eco.models.FileTrackInventory, ObjectId> {

}
