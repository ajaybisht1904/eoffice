package com.office.eco.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.office.eco.models.SignInventory;

@Repository
public interface SignInventoryRepository extends MongoRepository<SignInventory, ObjectId>{

}
