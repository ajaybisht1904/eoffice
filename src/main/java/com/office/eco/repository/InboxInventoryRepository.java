package com.office.eco.repository;

import java.util.List;
import java.util.Optional;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.office.eco.models.InboxInventory;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:28:26 PM
 *
 * office
 */
@Repository
public interface InboxInventoryRepository extends MongoRepository<InboxInventory, ObjectId> {

	public Page<InboxInventory> findAllByRoleNameOrderByRecieveDate(String id,Pageable pages);

//	public List<InboxInventory> findAllByisNewEntryAndRoleName(Boolean checkEntry, String roleName);

	public Optional<InboxInventory> findByisNewEntryAndFileInventoryId(Boolean checkEntry, String id);

	public List<InboxInventory> findBystatus(String status);

}
