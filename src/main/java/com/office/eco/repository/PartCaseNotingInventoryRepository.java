package com.office.eco.repository;

import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.office.eco.models.PartCaseNotingFile;

@Repository
public interface PartCaseNotingInventoryRepository extends MongoRepository<PartCaseNotingFile, ObjectId>{

	Optional<PartCaseNotingFile> findByFileId(String id);

	
}
