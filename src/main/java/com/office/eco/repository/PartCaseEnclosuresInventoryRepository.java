package com.office.eco.repository;

import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.office.eco.models.PartCaseEnclosureFile;

@Repository
public interface PartCaseEnclosuresInventoryRepository extends MongoRepository<PartCaseEnclosureFile, ObjectId>{

	Optional<PartCaseEnclosureFile> findByfileId(String fileId);


}
