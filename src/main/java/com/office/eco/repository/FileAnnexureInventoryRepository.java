package com.office.eco.repository;

import java.util.List;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.office.eco.models.FileAnnexureInventory;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:28:04 PM
 *
 * office
 */
@Repository
public interface FileAnnexureInventoryRepository extends MongoRepository<FileAnnexureInventory, ObjectId> {
	
	List<FileAnnexureInventory> findByPersonalFileId(String fileId);

	Optional<FileAnnexureInventory> findByContentId(String fileId);
}
