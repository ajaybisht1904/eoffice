package com.office.eco.repository;

import java.time.LocalDateTime;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.office.eco.models.OutboxInventory;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:28:36 PM
 *
 * office
 */
@Repository
public interface OutboxInventoryRepository extends MongoRepository<OutboxInventory, ObjectId> {

	public  Page<OutboxInventory> findAllByRoleNameOrderByDateSent(String id,Pageable pages);

	//public List<OutboxInventory> findbydateSent(LocalDateTime dateSent);
}
