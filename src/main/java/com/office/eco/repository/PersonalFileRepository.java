package com.office.eco.repository;

import java.util.List;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.office.eco.models.PersonalFileInventory;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:28:21 PM
 *
 * office
 */
@Repository
public interface PersonalFileRepository extends MongoRepository<PersonalFileInventory, ObjectId> {

	public  Page<PersonalFileInventory> findByroleNameOrderByCreatedOnDesc(String username,Pageable pages);
	public List<PersonalFileInventory> findByroleName(String role);
	public Optional<PersonalFileInventory> findByroleNameAndSubject(String role,String subject);
	//public List<PersonalFileInventory> findByfileName(String fileName);
}
