package com.office.eco.controller;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.mongodb.client.result.UpdateResult;
import com.office.eco.dto.AnnexureDto;
import com.office.eco.dto.InboxDTO;
import com.office.eco.dto.OutboxDTO;
import com.office.eco.dto.OutputBean1;
import com.office.eco.dto.PersonalApplicationDto;
import com.office.eco.dto.PersonalFileDto;
import com.office.eco.dto.SendFileDTO;
import com.office.eco.models.AnnotationData;
import com.office.eco.models.InboxCount;
import com.office.eco.models.InboxInventory;
import com.office.eco.models.PACount;
import com.office.eco.models.PartCaseEnclosureFile;
import com.office.eco.models.PartCaseNotingFile;
import com.office.eco.models.PersonalApplicationInventory;
import com.office.eco.models.PersonalFileInventory;
import com.office.eco.models.PersonalIdentityInventory;
import com.office.eco.models.PfileCount;
import com.office.eco.models.SignInventory;
import com.office.eco.repository.InboxInventoryRepository;
import com.office.eco.repository.OutboxInventoryRepository;
import com.office.eco.repository.PartCaseEnclosuresInventoryRepository;
import com.office.eco.repository.PartCaseNotingInventoryRepository;
import com.office.eco.repository.PersonalApplicationRepository;
import com.office.eco.repository.PersonalFileRepository;
import com.office.eco.repository.PersonalIdentityInventoryRepository;
import com.office.eco.repository.SignInventoryRepository;
import com.office.eco.service.AWSClientConfig;
import com.office.eco.service.RetrievalService;
import com.office.eco.service.SignNotingService;
 

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:25:37 PM
 *
 * office
 */
@RestController
@RequestMapping("/api")
public class RetrieveController {

	@Autowired
	MongoTemplate mongoTemplate; 
	@Autowired
	private SignNotingService iSignNoting01;
	@Autowired
	private InboxInventoryRepository inboxInventoryRepository;
	@Autowired
	private RetrievalService retrievalService;
	@Autowired
	private PersonalIdentityInventoryRepository personalidentityInventoryRepository;
	
	//	AWS Services
	@Autowired
	private PersonalFileRepository personalFileRepository;
	@Autowired
	private AWSClientConfig awsClientConfig;

	@Autowired
	private PersonalApplicationRepository personalApplicationRepository;
	
	@Value("${minio.rest-url}")
	private String baseUrl;
	
	@Value("${minio.rest-port}")
	private String port;
	
	private HashMap<String, Object> responseObj = new HashMap<String, Object>(2);
	
	@GetMapping("/getHrm")
	public ResponseEntity<HashMap<String, Object>> getHrm(HttpServletRequest request) {
		responseObj.clear();
		String personalId = (String) request.getHeader("fileId");
		HashMap<String, Object> json = retrievalService.getHrmData(personalId);
		return ResponseEntity.ok(json);
	}
	
	@GetMapping("/getAnnotation/{id}")
	public ResponseEntity<HashMap<String, Object>> getAnnotation(@PathVariable("id") String id,HttpServletRequest request)
	{
		responseObj.clear();
		String  xsdfString = retrievalService.getAnnotationData(id);
		HashMap<String, Object> json = new HashMap<String, Object>();
		if (xsdfString.length()>0) {
			json.put("status", HttpStatus.OK);
			json.put("xsdfString", xsdfString);
			return ResponseEntity.ok(json);
		} else {
			json.put("status", HttpStatus.NOT_FOUND);
			return ResponseEntity.ok(json);
		}
	}
	
	@PostMapping("/uploadAnnotation/{id}")
	public ResponseEntity<HashMap<String, Object>> uploadAnnotation(@RequestBody AnnotationData file,
			@PathVariable("id") String id, HttpServletRequest request)
	{
		responseObj.clear();
		String xsdfString =  file.getAnnotationData();
		boolean checkAnnotation = retrievalService.uploadAnnotationData(xsdfString, id);
		HashMap<String, Object> json = new HashMap<String, Object>();
		if (checkAnnotation) {
			json.put("status", HttpStatus.OK);
			return ResponseEntity.ok(json);
		} else {
			json.put("status", HttpStatus.NOT_FOUND);
			return ResponseEntity.ok(json);
		}
	}
	@Autowired
	private PartCaseNotingInventoryRepository partCaseNotingInventoryRepository;

	@Autowired
	private PartCaseEnclosuresInventoryRepository partCaseEnclosuresInventoryRepository;

	@Autowired
	private SignInventoryRepository signInventoryRepository;
	@PostMapping(value="/sign")
	public ResponseEntity<HashMap<String, Object>> signObject(@RequestParam("comments") String comments,@RequestParam("signTitle") String signTitle,@RequestParam("personalAppliactionFileId") String personalAppliactionFileId 
			,@RequestParam("tag") String tag,@RequestParam("pencilColorCode") String pencilColorCode,@RequestParam("username") String username
			,@RequestParam("dep_desc") String dep_desc,@RequestParam("color") String color,@RequestParam("url") String url,HttpServletRequest request) throws IOException
	{
		responseObj.clear();
		String token = (String) request.getHeader("Authorization");
		String roleName = (String) request.getHeader("roleName");
		String clippedToken = token.replace("Bearer ", "");
		
		URL url1=new URL(url+"?token="+clippedToken);
		InputStream input1 = new URL(url).openStream();
		OutputBean1 bytes1 = iSignNoting01.signNoting(comments,signTitle,input1,tag,pencilColorCode,username,dep_desc,color); 
		byte[] byteOut = bytes1.getResult();
		System.out.println(url);
		if(byteOut.length != 0)
		{
			ObjectId oldId = new ObjectId(personalAppliactionFileId);
			Optional<PersonalApplicationInventory> personalFile = personalApplicationRepository.findById(oldId);
			Optional<PartCaseNotingFile> notingFile = partCaseNotingInventoryRepository.findById(oldId);
			Optional<PartCaseEnclosureFile> encFile = partCaseEnclosuresInventoryRepository.findById(oldId);
			if(encFile.isPresent())
			{
				ObjectId newId =  new ObjectId();
				SignInventory newSign = SignInventory.builder().signedBy(roleName).signedOn(LocalDateTime.now()).id(newId)
						.build();
				signInventoryRepository.save(newSign);
				Query query = new Query();
				query.addCriteria(Criteria.where("id").is(personalAppliactionFileId));
				Update update = new Update();
				update.set("signedId", newId.toString());
				UpdateResult result = mongoTemplate.updateMulti(query, update, PartCaseEnclosureFile.class);
			}
			if(notingFile.isPresent())
			{
				ObjectId newId =  new ObjectId();
				SignInventory newSign = SignInventory.builder().signedBy(roleName).signedOn(LocalDateTime.now()).id(newId)
						.build();
				signInventoryRepository.save(newSign);
				Query query = new Query();
				query.addCriteria(Criteria.where("id").is(personalAppliactionFileId));
				Update update = new Update();
				update.set("signedId", newId.toString());
				UpdateResult result = mongoTemplate.updateMulti(query, update, PartCaseNotingFile.class);
			
			}
			if(personalFile.isPresent())
			{
				Query query = new Query();
				query.addCriteria(Criteria.where("id").is(personalAppliactionFileId));
				Update update = new Update();
				update.set("signedOn", LocalDateTime.now());
				UpdateResult result = mongoTemplate.updateMulti(query, update, PersonalApplicationInventory.class);
				
			}
		}
		ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream(byteOut.length);
		byteOutStream.write(byteOut, 0, byteOut.length);
		
		url = url.replace("%20", " ");
		String key = url;
		String newKey = "http://"+baseUrl+":"+port+"/"+roleName+"/";
		String newUrl = key.replace(newKey, "");
		String newURL ="";
		InputStream input11 =new ByteArrayInputStream(byteOut);
		try (XWPFDocument doc = new XWPFDocument(input11)) 
		{
			File initialFile = ResourceUtils.getFile(this.getClass().getResource("/").getPath()+"temp1.docx");
		try (FileOutputStream out = new FileOutputStream(initialFile)) {
			doc.write(out);
		}
		
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		
		//				File initialFile = new File();
		InputStream targetStream = new FileInputStream(initialFile);
		Path path = Paths.get(initialFile.getPath());
		long bytes = Files.size(path);
		AmazonS3 awsClient = awsClientConfig.awsClientConfiguration(clippedToken);
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
		metadata.setContentLength(bytes);
		DeleteObjectRequest requestDel = new DeleteObjectRequest(roleName, newUrl);
		PutObjectRequest request1 = new PutObjectRequest(roleName,newUrl, targetStream,metadata);
		awsClient.putObject(request1);
		}
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("url", url);
		return ResponseEntity.ok(json);
	
	}
	
	@PostMapping("/createFile")
	public ResponseEntity<HashMap<String, Object>> createFile(@RequestBody PersonalFileDto newFile) {
		responseObj.clear();
		HashMap<String, Object> json = retrievalService.createFile(newFile);
		return ResponseEntity.ok(json);
	}

//	Unique for All Roles
	@PostMapping("/updatePersonalInfo")
	public  ResponseEntity<HashMap<String, Object>> updatePersonalInfo(HttpServletRequest request,@RequestParam("roleName") String role,@PathParam("name") String name,@PathParam("email") String email,@PathParam("username") String username,@PathParam("address1") String address1,@PathParam("address2") String address2,@PathParam("address3") String address3,@PathParam("designation") String designation)
	{
		responseObj.clear();
		String token = (String) request.getHeader("Authorization");
		String clippedToken = token.replace("Bearer ", "");
		HashMap<String, Object> json = retrievalService.updatePersonalInfo(clippedToken,role,username, address1, address2, address3, name, email, designation);
		return ResponseEntity.ok(json);
	}
//	Unique for All Roles
	@PostMapping("/getPersonalInfo")
	public  ResponseEntity<HashMap<String, Object>> getPersonalInfo(@PathParam("username") String username)
	{
		responseObj.clear();
		HashMap<String, Object> json = retrievalService.getpersonalInfo(username);
		return ResponseEntity.ok(json);
	}
	
//	Unique for All Roles
	@PostMapping("/updateTemplate/{id}")
	public ResponseEntity<HashMap<String, Object>> uploadFile(@RequestParam("file") MultipartFile file,HttpServletRequest request,@PathVariable("id") String id) {
		responseObj.clear();
		String token = (String) request.getHeader("Authorization");
		String clippedToken = token.replace("Bearer ", "");
		String userName = (String) request.getHeader("userName");
		String roleName = (String) request.getHeader("roleName");
		boolean checkFile = retrievalService.updateFile(file, clippedToken,id);
		HashMap<String, Object> json = new HashMap<String, Object>();
		if (checkFile) {
			json.put("status", HttpStatus.OK);

			return ResponseEntity.ok(json);
		} else {
			json.put("status", HttpStatus.OK);
			return ResponseEntity.ok(json);
		}
	}


	@PostMapping("/sendFiles/{id}/{signed}")
	public ResponseEntity<HashMap<String, Object>> sendFiles(@PathVariable("id") String id,@PathVariable("signed") boolean signed, @RequestBody SendFileDTO sendFile,HttpServletRequest request) {
		responseObj.clear();
		String token = (String) request.getHeader("Authorization");
		String clippedToken = token.replace("Bearer ", "");
		String userName = (String) request.getHeader("userName");
		String roleName = (String) request.getHeader("roleName");

		retrievalService.sendFiles(id, sendFile,roleName,clippedToken,signed);
		// execute.notify(execution);
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("status", HttpStatus.OK);
		return ResponseEntity.ok(json);
	}
	
	@PostMapping("/sendFilesSection/{id}/{signed}")
	public ResponseEntity<HashMap<String, Object>> sendFilesSection(@PathVariable("id") String id,@PathVariable("signed") boolean signed, @RequestBody SendFileDTO sendFile,HttpServletRequest request) {
		responseObj.clear();
		String token = (String) request.getHeader("Authorization");
		String clippedToken = token.replace("Bearer ", "");
		String userName = (String) request.getHeader("userName");
		String roleName = (String) request.getHeader("roleName");

		retrievalService.sendFilesSection(sendFile,id,roleName,clippedToken,signed);
		// execute.notify(execution);
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("status", HttpStatus.OK);
		return ResponseEntity.ok(json);
	}
	
	@PostMapping("/sendFilesServiceNumber/{id}/{signed}")
	public ResponseEntity<HashMap<String, Object>> sendFilesServiceNumber(@PathVariable("id") String id,@PathVariable("signed") boolean signed, @RequestBody SendFileDTO sendFile,HttpServletRequest request) {
		responseObj.clear();
		String token = (String) request.getHeader("Authorization");
		String clippedToken = token.replace("Bearer ", "");
		String userName = (String) request.getHeader("userName");
		String roleName = (String) request.getHeader("roleName");

		retrievalService.sendFilesServiceNumber(sendFile,id,roleName,clippedToken,signed);
		// execute.notify(execution);
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("status", HttpStatus.OK);
		return ResponseEntity.ok(json);
	}

	@GetMapping("/getPFile")
	public ResponseEntity<HashMap<String, Object>> getPFile(HttpServletRequest request)
	{
		responseObj.clear();
		String roleName = request.getHeader("roleName");
		String pageNumber = (String) request.getHeader("pageNumber");
		String pageSize = (String) request.getHeader("pageSize");

		Pageable paging = PageRequest.of(Integer.parseInt(pageSize),Integer.parseInt(pageNumber));
		Page<PersonalFileInventory> pFileData = personalFileRepository
				.findByroleNameOrderByCreatedOnDesc(roleName,paging);
		Aggregation agg = newAggregation(
				match(Criteria.where("roleName").is(roleName)),
				group("roleName").count().as("total")
					
			);
		AggregationResults<PfileCount> groupResults 
		= mongoTemplate.aggregate(agg,PersonalFileInventory.class,PfileCount.class);
	List<PfileCount> result = groupResults.getMappedResults();
	for(PfileCount res:result)
	{
		
		responseObj.put("length", res.getTotal());
	}
		//List<PersonalFileInventory> pFileData = personalFileRepository.findByroleNameOrderByCreatedOnDesc(roleName);
		List<PersonalApplicationDto> personalApp = new ArrayList<PersonalApplicationDto>();
		for (PersonalFileInventory fileData:pFileData)
		{
			PersonalApplicationDto personalData = PersonalApplicationDto.builder().id(fileData.getId().toString()).pfileName(fileData.getFileName()).build();
			personalApp.add(personalData);
		}
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("status", HttpStatus.OK);
		json.put("data", personalApp);
		return ResponseEntity.ok(json);
	}

	@GetMapping("/getPersonalFile")
	public  ResponseEntity<HashMap<String, Object>> getPersonalFile(HttpServletRequest request)
	{
		responseObj.clear();
		String roleName = request.getHeader("roleName");
		int pageNumber = Integer.parseInt( request.getHeader("pageNumber"));
		int pageSize = Integer.parseInt( request.getHeader("pageSize"));
		List<PersonalFileInventory> personalFile = new ArrayList<>();
		Pageable paging = PageRequest.of(pageNumber,pageSize);
		Page<PersonalFileInventory> pFileData = personalFileRepository
				.findByroleNameOrderByCreatedOnDesc(roleName,paging);
		//personalFile.add(pFileData.getContent());
		Aggregation agg = newAggregation(
				match(Criteria.where("roleName").is(roleName)),
				group("roleName").count().as("total")
					
			);
		AggregationResults<PfileCount> groupResults 
		= mongoTemplate.aggregate(agg,PersonalFileInventory.class,PfileCount.class);
	List<PfileCount> result = groupResults.getMappedResults();
	System.out.println("pfiledata===="+pFileData);
	for(PfileCount res:result)
	{
		
		responseObj.put("length", res.getTotal());
	}

	List<PersonalFileInventory> personalApp = pFileData.getContent();
	System.out.println("pfiledata====="+personalApp);
	
		//List<PersonalFileInventory> pFileData = personalFileRepository.findByroleNameOrderByCreatedOnDesc(roleName);
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("status", HttpStatus.OK);
		json.put("data", personalApp);
		json.put("length",responseObj.get("length"));
		return ResponseEntity.ok(json);
	}

	@GetMapping("/getPersonalApplicationData")
	public ResponseEntity<HashMap<String, Object>> getPersonalApplicationData(HttpServletRequest request)
	{
		responseObj.clear();
		String roleName = request.getHeader("roleName");
		int pageNumber = Integer.parseInt( request.getHeader("pageNumber"));
		int pageSize = Integer.parseInt( request.getHeader("pageSize"));

		
		Pageable paging = PageRequest.of(pageNumber,pageSize);
		Page<PersonalApplicationInventory> pFileData = personalApplicationRepository
				.findByStatusAndRoleNameOrderByCreatedOnDesc("Sent",roleName,paging);
		Aggregation agg = newAggregation(
				match(Criteria.where("roleName").is(roleName)),
				group("roleName").count().as("total")
					
			);
		AggregationResults<PACount> groupResults 
		= mongoTemplate.aggregate(agg, PersonalApplicationInventory.class,PACount.class);
	List<PACount> result = groupResults.getMappedResults();
	System.out.println("pfiledata==="+pFileData);
	for(PACount res:result)
	{
		
		responseObj.put("length", res.getTotal());
	}

		//List<PersonalApplicationInventory> pFileData = personalApplicationRepository.findByStatusAndRoleNameOrderByCreatedOnDesc("Sent",roleName);
		List<PersonalApplicationDto> personalApp = new ArrayList<PersonalApplicationDto>();
		for (PersonalApplicationInventory fileData:pFileData)
		{
			LocalDateTime createdOn = fileData.getCreatedOn();
			DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");  
			String formatDateTime = createdOn.format(format);   
			PersonalApplicationDto personalData = PersonalApplicationDto.builder().subject(fileData.getSubject()).id(fileData.getId().toString()).status(fileData.getStatus()).fileURL(fileData.getFileURL()).pfileName(fileData.getPfileName()).createdOn(formatDateTime).build();
			personalApp.add(personalData);
		}
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("status", HttpStatus.OK);
		json.put("data", personalApp);
		json.put("length",responseObj.get("length"));
		return ResponseEntity.ok(json);
	}

	@PostMapping("/createApplication")
	public ResponseEntity<HashMap<String, Object>> createApplication(@RequestBody PersonalApplicationInventory newFile, HttpServletRequest request) {
		responseObj.clear();
		String token = (String) request.getHeader("Authorization");
		String clipToken = token.replace("Bearer ", "");
		String roleName = (String) request.getHeader("roleName");
		String grp = (String) request.getHeader("grp");
		retrievalService.createPersonalApplication(newFile, clipToken, roleName,grp);
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("status", HttpStatus.OK);
		return ResponseEntity.ok(json);
	}	

	@SuppressWarnings("unchecked")
	@GetMapping("/checkDraftFile/{id}")
	public ResponseEntity<HashMap<String, Object>> checkDraftFile(@PathVariable("id") String id) {
		responseObj.clear();
		retrievalService.checkDraftFile(id);
		// execute.notify(execution);
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("status", HttpStatus.OK);
		return ResponseEntity.ok(json);
	}

	@SuppressWarnings("unchecked")
	@GetMapping("/checkFile/{id}")
	public ResponseEntity<HashMap<String, Object>> checkFile(@PathVariable("id") String id) {
		responseObj.clear();
		retrievalService.checkFile(id);
		// execute.notify(execution);
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("status", HttpStatus.OK);
		return ResponseEntity.ok(json);
	}

	@SuppressWarnings("unchecked")
	@GetMapping("/getDraftData")
	public ResponseEntity<HashMap<String, Object>> getDraftData(HttpServletRequest request) {
		responseObj.clear();
		String roleName = request.getHeader("roleName");
		String pageNumber = (String) request.getHeader("pageNumber");
		String pageSize = (String) request.getHeader("pageSize");
		
		Pageable paging = PageRequest.of(Integer.parseInt(pageSize),Integer.parseInt(pageNumber));
	      
		Page<PersonalApplicationInventory> pFileData = personalApplicationRepository
				.findByStatusAndRoleNameOrderByCreatedOnDesc("In Progress",roleName,paging);
		Aggregation agg = newAggregation(
				match(Criteria.where("roleName").is(roleName)),
				group("roleName").count().as("total")
					
			);
		AggregationResults<PACount> groupResults 
		= mongoTemplate.aggregate(agg, PersonalApplicationInventory.class,PACount.class);
	List<PACount> result = groupResults.getMappedResults();
	for(PACount res:result)
	{
		
		responseObj.put("length", res.getTotal());
	}

		//List<PersonalApplicationInventory> pFileData = personalApplicationRepository.findByStatusAndRoleNameOrderByCreatedOnDesc("In Progress",roleName);
		List<PersonalApplicationDto> personalApp = new ArrayList<PersonalApplicationDto>();
		for (PersonalApplicationInventory fileData:pFileData)
		{
			LocalDateTime createdOn = fileData.getCreatedOn();
			DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");  
			String formatDateTime = createdOn.format(format);   
			PersonalApplicationDto personalData = PersonalApplicationDto.builder().subject(fileData.getSubject()).id(fileData.getId().toString()).status(fileData.getStatus()).fileURL(fileData.getFileURL()).pfileName(fileData.getPfileName()).createdOn(formatDateTime).build();
			personalApp.add(personalData);
		}
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("status", HttpStatus.OK);
		json.put("data", personalApp);
		return ResponseEntity.ok(json);
	}


	@SuppressWarnings("unchecked")
	@GetMapping("/getInboxData")
	public ResponseEntity<HashMap<String, Object>> getInboxData(HttpServletRequest request) {
		responseObj.clear();
		String roleName = (String) request.getHeader("roleName");
		String userName = (String) request.getHeader("userName");
		String pageNumber = (String) request.getHeader("pageNumber");
		String pageSize = (String) request.getHeader("pageSize");
		HashMap<String, Object> inboxDataList = retrievalService.getInboxData(roleName,userName,Integer.parseInt(pageSize),Integer.parseInt(pageNumber));

		
		
		return ResponseEntity.ok(inboxDataList);
	}

	@SuppressWarnings("unchecked")
	@GetMapping("/getInboxDataSplitView/{id}")
	public ResponseEntity<HashMap<String, Object>> getInboxDataSplitView(HttpServletRequest request, @PathVariable("id") String id) {
		responseObj.clear();
		System.out.println();
		ObjectId inboxId = new ObjectId(id);
		Optional<InboxInventory> inboxData = inboxInventoryRepository.findById(inboxId);
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("status", HttpStatus.OK);
		json.put("Data", inboxData.get());
		return ResponseEntity.ok(json);
	}
	

	@SuppressWarnings("unchecked")
	@GetMapping("/getOutboxData")
	public ResponseEntity<HashMap<String, Object>> getOutboxData(HttpServletRequest request) {
		responseObj.clear();
		String roleName = (String) request.getHeader("roleName");
		String userName = (String) request.getHeader("userName");
		String pageNumber = (String) request.getHeader("pageNumber");
		String pageSize = (String) request.getHeader("pageSize");
		HashMap<String, Object> outboxDataList = retrievalService.getOutboxData(roleName,userName,Integer.parseInt(pageSize),Integer.parseInt(pageNumber));

//		HashMap<String, Object> json = new HashMap<String, Object>();
//		json.put("status", HttpStatus.OK);
//		json.put("Data", outboxDataList);
		return ResponseEntity.ok(outboxDataList);
	}

	@PostMapping("/saveDocument/{id}")
	public ResponseEntity<HashMap<String, Object>> saveDocument(@PathParam("file") MultipartFile file, @PathVariable("id")String id,HttpServletRequest request) throws IOException {
		responseObj.clear();
		String token = (String) request.getHeader("Authorization");
		String clipToken = token.replace("Bearer ", "");
		String roleName = (String) request.getHeader("roleName");
		String isPartCase = request.getHeader("isPartCase");
		boolean isPartCase1 = Boolean.parseBoolean(isPartCase);  
		System.out.println("Is PartCase"+ isPartCase1);
		boolean checkFile = retrievalService.saveDocument(file,id, clipToken, roleName,isPartCase1);
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("status", Boolean.toString(checkFile));
		return ResponseEntity.ok(json);
	}
	
	@PostMapping("/getAnnexureData/{id}")
	public ResponseEntity<HashMap<String, Object>> getAnnexureFile(HttpServletRequest request,@PathVariable("id") String id) 
	{
		responseObj.clear();
		List<AnnexureDto> listData = retrievalService.getAnnexureFile(id);
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("status", HttpStatus.OK);
		json.put("data", listData);
		return ResponseEntity.ok(json);
	}

	@SuppressWarnings("unchecked")
	@PostMapping("/upload/annexure/file/{id}")
	public ResponseEntity<HashMap<String, Object>> uploadAnnexureFile(@RequestParam("file") MultipartFile[] file,HttpServletRequest request,@PathVariable("id") String id) {
		responseObj.clear();
		String token = (String) request.getHeader("Authorization");
		String clippedToken = token.replace("Bearer ", "");
		String userName = (String) request.getHeader("userName");
		String roleName = (String) request.getHeader("roleName");
		boolean checkFile = retrievalService.uploadAnnexureFile(file, clippedToken, userName, roleName, id);
		HashMap<String, Object> json = new HashMap<String, Object>();
		if (checkFile) {
			json.put("status", HttpStatus.OK);

			return ResponseEntity.ok(json);
		} else {
			json.put("status", HttpStatus.OK);

			return ResponseEntity.ok(json);
		}
	}

	@GetMapping("/sfdt")
	public ResponseEntity<HashMap<String, Object>> uploadFile(HttpServletRequest request) throws Exception {
		responseObj.clear();
		try {
			String url = (String) request.getHeader("url");
			String fileId = (String) request.getHeader("fileId");
			String username = (String) request.getHeader("userName");
			String roleName = (String) request.getHeader("roleName");
			String grp = (String) request.getHeader("groupName");
			ObjectId newId = new ObjectId(fileId);
			Optional<PersonalApplicationInventory> personalData1 = personalApplicationRepository.findById(newId);
			Optional<PersonalIdentityInventory> personalData = personalidentityInventoryRepository.findByUsername(username);
			
			String name  = personalData.get().getName();
			String designation = personalData.get().getDesignation();
			String address1 = personalData.get().getAddress1();
			String address2 = personalData.get().getAddress2();
			String address3 = personalData.get().getAddress3();
			String email = personalData.get().getEmail();

			String token = (String) request.getHeader("Authorization");
			String clippedToken = token.replace("Bearer ", "");
			URL url1=new URL(url+"?token="+clippedToken);
			InputStream input1 = new URL(url).openStream();
			
			try (XWPFDocument doc = new XWPFDocument(input1)) 
			{
				
				
				List<XWPFParagraph> xwpfParagraphList = doc.getParagraphs();
				//Iterate over paragraph list and check for the replaceable text in each paragraph
				
				for (XWPFParagraph xwpfParagraph : xwpfParagraphList) {
					for (XWPFRun xwpfRun : xwpfParagraph.getRuns()) {
						String docText = xwpfRun.getText(0);
						
						if(docText != null)
						{
							System.out.println(docText);
						docText = docText.replace("[Subject]", personalData1.get().getSubject());
						docText = docText.replace("[Name]", name);
						docText = docText.replace("[Designation]", designation);
						docText = docText.replace("[Email]", email);
						docText = docText.replace("[Address1]", address1);
						docText = docText.replace("[Address2]", address2);
						docText = docText.replace("[Address3]", address3);
						xwpfRun.setText(docText, 0);
						}
					}
				}
				// save the docs
				String key = url1.getPath();
				String newKey = key.replaceAll(".*?\\/"+baseUrl+":"+port+"\\/.*?\\/","");
				File initialFile = ResourceUtils.getFile(this.getClass().getResource("/").getPath()+"temp.docx");
				try (FileOutputStream out = new FileOutputStream(initialFile)) {
					doc.write(out);
				}
				catch(Exception ex)
				{
					System.out.println(ex);
				}

				//				File initialFile = new File();
				InputStream targetStream = new FileInputStream(initialFile);
				Path path = Paths.get(initialFile.getPath());
				long bytes = Files.size(path);
				AmazonS3 awsClient = awsClientConfig.awsClientConfiguration(clippedToken);
				ObjectMetadata metadata = new ObjectMetadata();
				metadata.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
				metadata.setContentLength(bytes);

				PutObjectRequest request1 = new PutObjectRequest(roleName,grp+"/"+personalData1.get().getSubject()+"/"+personalData1.get().getPfileName()+"/PersonalTemplate.docx", targetStream,metadata);
				awsClient.putObject(request1);

			}

			HashMap<String, Object> json = new HashMap<String, Object>();
			json.put("url", url1);
			return ResponseEntity.ok(json);
		} 
		catch (Exception e) {
			e.printStackTrace();
			String msg = "{\"sections\":[{\"blocks\":[{\"inlines\":[{\"text\":" + e.getMessage() + "}]}]}]}";
			HashMap<String, Object> json = new HashMap<String, Object>();
			json.put("sfdt", msg);
			return ResponseEntity.ok(json);
		}
	}

	@PostMapping("/deleteAnnexureData/{id}")
	public ResponseEntity<HashMap<String, Object>> deleteAnnexureFile(HttpServletRequest request,@PathVariable("id") String id)
	{
		responseObj.clear();
		HashMap<String, Object> json  = retrievalService.deleteAnnexureFile(id);
		return ResponseEntity.ok(json);
	}
	
	@PostMapping("/createPANotingData/{id}")
	public ResponseEntity<HashMap<String, Object>> createPANotingData(HttpServletRequest request,@PathVariable("id") String id) throws Exception 
	{
		responseObj.clear();
		String token = request.getHeader("Authorization");
		String clipToken = token.replace("Bearer ", "");
		String roleName = request.getHeader("roleName");
		String grp = request.getHeader("grp");
		HashMap<String, Object> json  = retrievalService.getPANoting(id, roleName, grp, clipToken);
		return ResponseEntity.ok(json);
	}
	
	@PostMapping("/createPartCaseFile")
	public ResponseEntity<HashMap<String, Object>> createPartCaseFile(HttpServletRequest request, @RequestParam("inboxId") String inboxId,@RequestParam("applicationId") String applicationId,@RequestParam("fileNumber") String fileName) throws IOException
	{
		responseObj.clear();
		String token = request.getHeader("Authorization");
		String clipToken = token.replace("Bearer ", "");
		String roleName = request.getHeader("roleName");
		String username = request.getHeader("username");
		String grp = request.getHeader("grp");
		retrievalService.createPartCaseFile(clipToken, grp, fileName, username, roleName, applicationId, inboxId);
		responseObj.put("status", HttpStatus.OK);
		return ResponseEntity.ok(responseObj);
	}
	
	@PostMapping("/getPartCaseData")
	public ResponseEntity<HashMap<String, Object>> getPartCaseData(HttpServletRequest request,@RequestParam("id") String id) throws Exception 
	{
		responseObj.clear();
		String token = request.getHeader("Authorization");
		String clipToken = token.replace("Bearer ", "");
		HashMap<String, Object> json  = retrievalService.getPartCaseData(id);
		return ResponseEntity.ok(json);
	}
}
