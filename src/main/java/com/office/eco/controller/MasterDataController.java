package com.office.eco.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.office.eco.enums.Classification;
import com.office.eco.enums.Status;
import com.office.eco.enums.Type;
import com.office.eco.keycloakModels.KeycloakGroupRoleMapping;
import com.office.eco.keycloakModels.KeycloakGroups;
import com.office.eco.keycloakModels.KeycloakRoles;
import com.office.eco.keycloakModels.KeycloakUsers;
import com.office.eco.keycloakRepo.KeycloakClientRepository;
import com.office.eco.keycloakRepo.KeycloakGroupRepository;
import com.office.eco.keycloakRepo.KeycloakGroupRoleMappingRepository;
import com.office.eco.keycloakRepo.KeycloakRolsRepository;
import com.office.eco.keycloakRepo.KeycloakUsersRepository;

/**
 * @author Aman Gupta
 *
 * Date : Nov 5, 2020 Time : 12:25:28 PM
 *
 * office
 */
@RestController
@RequestMapping("/api")
public class MasterDataController {

	@Autowired
	private KeycloakRolsRepository keycloakRolsRepository;

	@Autowired
	private KeycloakClientRepository keycloakClientRepository;

	@Autowired
	private KeycloakGroupRepository keycloakGroupRepository;

	@Autowired
	private KeycloakGroupRoleMappingRepository keycloakGroupRoleMappingRepository;
	
	@Autowired
	private KeycloakUsersRepository keycloakUsersRepository;

	@SuppressWarnings("unchecked")
	@GetMapping("/getFileClassification")
	public ResponseEntity<JSONObject> getFileClassification() {
		JSONObject json = new JSONObject();
		json.put("status", HttpStatus.OK);
		json.put("data", Classification.values());
		return ResponseEntity.ok(json);
	}

	@SuppressWarnings("unchecked")
	@GetMapping("/getFileStatus")
	public ResponseEntity<JSONObject> getFileStatus() {

		JSONObject json = new JSONObject();
		json.put("status", HttpStatus.OK);
		json.put("data", Status.values());
		return ResponseEntity.ok(json);
	}

	@SuppressWarnings("unchecked")
	@GetMapping("/getFileType")
	public ResponseEntity<JSONObject> getFileType() {

		JSONObject json = new JSONObject();
		json.put("status", HttpStatus.OK);
		json.put("data", Type.values());
		return ResponseEntity.ok(json);
	}


	@GetMapping("/getUserRoles")
	public ResponseEntity<JSONObject> getUserRoles(HttpServletRequest request) {
		String grpName = request.getHeader("groupName");
		Optional<KeycloakGroups> grpData = keycloakGroupRepository.findBygrpName(grpName);
		List<String> rolesList = new ArrayList<String>();
		if(grpData.isPresent())
		{
			List<KeycloakGroupRoleMapping> grpsData = keycloakGroupRoleMappingRepository.findByGroupId(grpData.get().getGrpId());
			
			for (KeycloakGroupRoleMapping roleData:grpsData)
			{
				System.out.println(roleData.getRoleId());
				Optional<KeycloakRoles> rolesData = keycloakRolsRepository.findById(roleData.getRoleId());
				rolesList.add(rolesData.get().getRoleName());
			}
		}
		JSONObject json = new JSONObject();
		json.put("status", HttpStatus.OK);
		json.put("data", rolesList);
		return ResponseEntity.ok(json);

	}

	@GetMapping("/getSection")
	public ResponseEntity<JSONObject> getSection(HttpServletRequest request) throws InterruptedException {
		List<KeycloakGroups> grpData = keycloakGroupRepository.findAll();
		JSONObject json = new JSONObject();
		json.put("status", HttpStatus.OK);
		json.put("data", grpData);
		return ResponseEntity.ok(json);
	}

	@GetMapping("/getServiceNumber")
	public ResponseEntity<JSONObject> getServiceNumber(HttpServletRequest request) throws InterruptedException {
		List<KeycloakUsers> userData = keycloakUsersRepository.findAll();
		JSONObject json = new JSONObject();
		json.put("status", HttpStatus.OK);
		json.put("data", userData);
		return ResponseEntity.ok(json);
	}

}
