From tomcat:jre8-openjdk
COPY ./target/office-2.4.0.war /usr/local/tomcat/webapps/office.war
CMD ["catalina.sh","run"]